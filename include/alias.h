#ifndef ALIAS_H
# define ALIAS_H

/**
 * \file lexer.c
 *
 * Manage the expansion of alias
 */

/**
 * \fn aliasing(char *word)
 * \brief expand if possible alias on this string
 *
 * \param the string to transform
 *
 * \return allocated string of the word expanded with stocked alias
 */
char *aliasing(char *word);

#endif /* !ALIAS_H */
