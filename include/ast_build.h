#ifndef AST_BUILD_H
# define AST_BUILD_H

/**
 * \file ast_build.c
 *
 * Build the AST of the input command
 */

/**
 * \fn init_ast_shell(int tag)
 * \brief initialize a struct ast_shell
 *
 * \param tag which can of ast_shell should be initialize
 *
 * \return the initialized ast_shell
 */
struct ast_shell *init_ast_shell(int tag, unsigned neg);

/**
 * \fn build_shell_ast(void)
 * \brief build the next ast_shell
 *
 * \param first should the PS1 be printed
 *
 * \return ast_shell of the next groups of commands
 */
struct ast_shell *build_ast_shell(int first, int onlyone);

#endif /* !AST_BUILD_H */
