#ifndef AST_SHELL_H
# define AST_SHELL_H

enum ast_type
{
  A_FOR,
  A_WHILE,
  A_IF,
  A_PIPE,
  A_DO,
  A_CMD, // 5
  A_ASSIG,
  A_DONE,
  A_ELSE,
  A_FI,
  A_OPE, // 10
  A_CASE,
  A_DELIM,
  A_FUNC,
  A_CLOSE,
  A_BRACKCL, // 15
  A_LINE,
  A_THEN,
  A_ELIF,
  A_ESUALC,
  A_ESAC // 20
};

enum source_type
{
  FILE_SRC,
  INTERACTIVE,
  CMD_SRC
};

/**
 * \struct astfor
 */
struct astfor
{
  char *var; /*!< variable to update */
  char **in; /*!< value to update the variable with */
  unsigned nb_in; /*!< number of elements in 'in'*/
  struct ast_shell *body; /*!< command to execute in the for*/
};

/**
 * \struct astwhile
 */
struct astwhile
{
  struct ast_shell *test; /*!< ast_shell to execute for the test*/
  struct ast_shell *body; /*!< ast_shell to execute in the while/until */
};

/**
 * \struct clause
 */
struct clause
{
  char **test; /*!< different words to test */
  struct ast_shell *body; /*!< ast_shell to execute in the case */
  struct clause *next; /*!< next element of the list */
};

/**
 * \struct astcase
 */
struct astcase
{
  char *word; /*!< word to test in the case */
  struct clause *head; /*!< head of a list of the different clause of the case*/
};

/**
 * \struct astif
 */
struct astif
{
  struct ast_shell *test; /*!< ast_shell to execute for the test*/
  struct ast_shell *then_body; /*!< ast_shell to execute for the 'then' */
  struct ast_shell *else_body; /*!< ast_shell to execute for the 'else' */
};

/**
 * \struct astpipe
 * NOT IMPLEMENTED
 */
struct astpipe
{
  struct ast_shell *left_part; /*!< */
  struct ast_shell *right_part; /*!< */
};

/**
 * \struct astope
 */
struct astope
{
  int tag; /*!< token TOK_AND or TOK_OR */
  struct ast_shell *left_part; /*!< left part of the operator to execute */
  struct ast_shell *right_part; /*!< right part of the operator to execute */
};

/**
 * \struct redi_list
 * NOT USED
 */
struct redi_list
{
  int fd_in;
  int fd_out;
  struct redi_list *next;
};

/**
 * \struct astassig
 */
struct astassig
{
  char *var; /*!< name of the variable */
  char *value; /*!< value of the variable */
};

/**
 * \struct astcmd
 */
struct astcmd
{
  char **cmd; /*!< list of arguments to execute with execvp */
  unsigned argc; /*!< number of arguments */
  struct redi_list *redirect; /*!< NOT USED */
};

/**
 * \struct astdelim
 */
struct astdelim
{
  struct ast_shell *body; /*!< ast_shell of '('...')' or '{'...'}' to execute */
};

/**
 * \struct astfunc
 */
struct astfunc
{
  char *name; /*!< name of the function */
  struct ast_shell *body; /*!< ast_shell of the function to execute */
};

union ast_core
{
  struct astfor astfor;
  struct astwhile astwhile;
  struct astif astif;
  struct astpipe astpipe;
  struct astassig astassig;
  struct astcmd astcmd;
  struct astope astope;
  struct astcase astcase;
  struct astdelim astdelim;
  struct astfunc astfunc;
};

/**
 * \struct ast_shell
 */
struct ast_shell
{
  int tag; /*!< type of ast_shell */
  unsigned neg; /*!< number of negation in front of the ast_shell */
  union ast_core core; /*!< particular data to each kind of ast_shell */
  struct ast_shell *next; /*!< next ast_shell to execute in the list */
};

#endif /* !AST_SHELL_H */
