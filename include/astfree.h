#ifndef ASTFREE_H
# define ASTFREE_H

/**
 * \file astfree.c
 *
 * Manage the liberation of allocation for the different ast_shell
 */

struct clause;

/**
 * \fn free_array(char **s, int nb)
 * \brief free 's' and it's data
 *
 * \param s the array to free
 * \param nb the number of slot, -1 if null terminated
 */
void free_array(char **s, int nb);

/**
 * \fn free_case(struct clause *list, int runtime)
 * \brief free struct clause
 *
 * \param list the clause to free
 * \param runtime is the free done during the execution ? 0 -> no, 1 otherwise
 */
void free_case(struct clause *list, int runtime);

/**
 * \fn free_ast_shell(struct ast_shell *ast)
 * \brief free the ast_shell struct
 *
 * \param ast ast_shell to free
 * \param runtime is the free done during the execution ? 0 -> no, 1 otherwise
 */
void free_ast_shell(struct ast_shell *ast, int runtime);

#endif /* !ASTFREE_H */
