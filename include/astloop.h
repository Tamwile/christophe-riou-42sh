#ifndef ASTLOOP_H
# define ASTLOOP_H

/**
 * \file astloop.c
 *
 * Build the AST for loops 'for', 'while' and 'until'
 */

/**
 * \fn build_loop(struct ast_shell *tmp, int stop1, int stop2, int *tag)
 * \brief add all the consecutive command to the next of tmp
 *
 * \param tmp the head of the list
 * \param stop1 the type of the ast_shell that should finish the list
 * \param stop2 an other type of ast_shell that could finish the list
 * \param tag the type of ast_shell which really ended the list
 * \return !=0 if an error occured, 0 otherwize
 */
int build_loop(struct ast_shell *tmp, int stop1, int stop2, int *tag);

/**
 * \fn build_ast_while(unsigned neg, int type)
 * \brief build AST for 'while' or 'until'
 *
 * \param neg the number of negation in front of the command
 * \param type 0 for 'while', 1 for 'until'
 * \return a valid struct ast_shell, NULL otherwize
 */
struct ast_shell *build_ast_while(unsigned neg, int type);

/**
 * \fn build_ast_for(unsigned neg)
 * \brief build AST for 'for'
 *
 * \param neg the number of negation in front of the command
 * \return a valid struct ast_shell, NULL otherwize
 */
struct ast_shell *build_ast_for(unsigned neg);

#endif /* !ASTLOOP_H */
