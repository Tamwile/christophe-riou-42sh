#ifndef ASTTEST_H
# define ASTTEST_H

/**
 * \file asttest.c
 *
 * Build the AST for test 'if' and  'case'
 */

/**
 * \fn build_ast_if(unsigned neg)
 * \brief build AST for 'if'
 *
 * \param neg the number of negation in front of the command
 * \return a valid struct ast_shell, NULL otherwize
 */
struct ast_shell *build_ast_if(unsigned neg);

/**
 * \fn build_ast_case(unsigned neg)
 * \brief build AST for 'case'
 *
 * \param neg the number of negation in front of the command
 * \return a valid struct ast_shell, NULL otherwize
 */
struct ast_shell *build_ast_case(unsigned neg);

#endif /* !ASTTEST_H */
