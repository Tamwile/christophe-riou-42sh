#ifndef BLT_BLOCK_H
# define BLT_BLOCK_H

/**
 * \file blt_block.c
 *
 * Builtins exit, break and continue
 */

/**
 * \fn blt_block(char **args, unsigned argc, int *state, int loops)
 * \brief call the correct builtins
 *
 * \param command to execute
 * \param argc number of arguments in the command
 * \param state state of execution, for exit especialy
 * \param loops depth of loops
 * \return return value of the builtin
 */
int blt_block(char **args, unsigned argc, int *state, int loops);

#endif /* !BLT_BLOCK_H */
