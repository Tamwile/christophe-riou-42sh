#ifndef BUILTIN_H
# define BUILTIN_H

/**
 * \file builtin.c
 *
 * Manage the builtin of 42sh
 */

enum builtin_type
{
  B_EXIT,
  B_CD,
  B_SHOPT,
  B_EXPORT,
  B_ALIAS,
  B_UNALIAS,
  B_ECHO,
  B_CONTINUE,
  B_BREAK,
  B_SOURCE,
  B_HISTORY
};

/**
 * \fn get_builtin_id(char *cmd)
 * \brief return the id of the builtin cmd
 *
 * \param cmd possible builtin
 * \return the id of the apropriate builtin, -1 otherwize
 */
int get_builtin_id(char *cmd);

/**
 * \fn exec_builtin(char **args, unsigned argc, int *state, int loops)
 * \brief call the correct builtins
 *
 * \param command to execute
 * \param argc number of arguments in the command
 * \param state state of execution, for exit especialy
 * \param loops depth of loops
 * \return return value of the builtin
 */
int exec_builtin(char **args, unsigned argc, int *state, int loops);

#endif /* !BUILTIN_H */
