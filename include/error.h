#ifndef ERROR_H
# define ERROR_H

/**
 * \file error.c
 *
 * Manage the sanity of the syntax and the message error
 */

/**
 * \fn print_err_tok(int tok)
 * \brief print the apropriate message of error for the token tok
 *
 * \param tok token to print on stderr
 */
void print_err_tok(int tok);

/**
 * \fn unexpected(int tag, int expect1, int expect2)
 * \brief raise an error if tag is incorrect
 *
 * \param tag 'A_*' to check
 * \param expect1 allowed type
 * \param expect2 another allowed type
 * \return 0 if there is no problem
 */
int unexpected(int tag, int expect1, int expect2);

#endif /* !ERROR_H */
