#ifndef EXPAND_H
# define EXPAND_H

/**
 * \file expand.c
 *
 * Expand variables from variables table
 */

/**
 * \fn merge(char **a, char **b)
 * \brief merge the array 'a' and 'b'
 *
 * \param a first array to merge
 * \param b second array to merge
 *
 * \return the array a + b
 */
char **merge(char **a, char **b);

/**
 * \fn build_words_expansion(char **args, int erase)
 * \brief expand the variable in the string of the array args
 *
 * \param args string to expand
 * \param erase should the quote be supress
 *
 * \return the array of the expanded string
 */
char **build_words_expansion(char **args, int erase);

char *expand_string(char *str);

#endif /* !EXPAND_H */
