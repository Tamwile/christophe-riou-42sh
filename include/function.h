#ifndef FUNCTION_H
# define FUNCTION_H

/**
 * \file function.c
 *
 * build the AST for 'function'
 */

/**
 * \fn build_ast_function(unsigned neg, char *name)
 * \brief build AST for 'function'
 *
 * \param neg the number of negation in front of the command
 * \param name of the function, NULL if it haven't been parse yet
 * \return a valid struct ast_shell, NULL otherwize
 */
struct ast_shell *build_ast_function(unsigned neg, char *name);

#endif /* !FUNCTION_H */
