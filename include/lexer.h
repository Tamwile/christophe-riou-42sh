#ifndef LEXER_H
# define LEXER_H

/**
 * \file lexer.c
 *
 * Determine what is the next token to read.
 */

enum token_id
{
  TOK_HIDE_FUNC = -6,
  TOK_ASSIG = -5,
  TOK_VOID = -4,
  TOK_LINE = -3,
  TOK_SPACE = -2,
  TOK_WORD = -1,
  TOK_FOR,
  TOK_IN,
  TOK_DO,
  TOK_WHILE,
  TOK_UNTIL,
  TOK_DONE, // 5
  TOK_IF,
  TOK_THEN,
  TOK_ELSE,
  TOK_FI,
  TOK_OPEN, // 10
  TOK_CLOSE,
  TOK_AND,
  TOK_OR,
  TOK_SEMIC,
  TOK_NEG, // 15
  TOK_ESUALC,
  TOK_PIPE,
  TOK_CASE,
  TOK_ESAC,
  TOK_FUNC, // 20
  TOK_BRACKOP,
  TOK_BRACKCL,
  TOK_PARENTH,
  TOK_ELIF,
  TOK_AMPERSAND // 25
};

/**
 * \fn get_next_token(char **word, int nonblock)
 * \brief return the next token
 *
 * \param word if the token is a word, it is stored in word
 * \param nonblock should the PS2 be prompt if the reading of token stop
 * \param raw 0 the word is liberated if it is a key word, 1 there is no free
 * \return the id of the next token
 */
int get_next_token(char **word, int nonblock, int raw);

/**
 * \fn get_full_word(char *next, unsigned *nb)
 * \brief read all next words
 *
 * \param next last word readed
 * \param nb store the number of words returned in nb
 * \return the list of every words
 */
char **get_full_word(char *next, unsigned *nb);

/**
 * \fn get_next_var(void)
 * \brief read the next variable
 *
 * \param error should an error be printed
 *
 * \return the next variable or NULL if it not correct
 */
char *get_next_var(int error);

#endif /* !LEXER_H */
