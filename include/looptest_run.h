#ifndef LOOPTEST_RUN_H
# define LOOPTEST_RUN_H

/**
 * \file looptest_run.c
 *
 * execute the AST of type 'for', 'while', 'until' 'if' and 'case'
 */

/**
 * \fn run_looptest(struct ast_shell *ast, int *state, int loops)
 * \brief execute the correct AST
 *
 * \param ast AST to execute
 * \param state state of the execution
 * \param loops number of loops of depth
 *
 * \return return value of the executed AST
 */
int run_looptest(struct ast_shell *ast, int *state, int loops);

#endif /* !LOOPTEST_RUN_H */
