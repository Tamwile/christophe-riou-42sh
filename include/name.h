#ifndef NAME_H
# define NAME_H

/**
 * \file name.h
 *
 * Manage token name and variable sanity
 */

/**
 * \fn get_token_name(int tok)
 * \brief return the string representation of the token
 *
 * \param tok token to print
 *
 * \return string representing the token
 */
char *get_token_name(int tok);

/**
 * \fn check_var(char *var)
 * \brief check that var is a valid variable name
 *
 * \param var the variable to check
 *
 * \return 1 if it is valid, 0 otherwise
 */
int check_var(const char *var);

#endif /* !NAME_H */
