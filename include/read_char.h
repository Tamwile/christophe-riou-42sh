#ifndef READ_CHAR_H
# define READ_CHAR_H

/**
 * \file read_char.c
 *
 * Determine what is the next char to read
 */

/**
 * \struct source
 */
struct source
{
  int tag; /*!< type of execution -> interacive, ect .. */
  FILE *file; /*!< file source */
  char *cmd; /*!< -c -> cmd to execute */
  unsigned cursor; /*!< cursor where we have to read in the cmd */
  int end; /*!< is the file/cmd finished */
  int ahead; /*!< save the next char */
  int mem_tok; /*!< save the last token */
  char *buf; /*!< save the line read */
  int error; /*!< message of the latest error when parsing */
};

/**
 * \fn get_next_char(int nonblock)
 * \brief give the next character
 *
 * \param should the PS2 be prompt if necessary
 *
 * \return the next character to read
 */
char get_next_char(int nonblock);

/**
 * \fn actualise(void);
 * \brief actualise the char g_src.ahead if it is not set
 */
void actualise(void);

/**
 * \fn mem_token(int tok)
 * \brief memorise a token for the next call of get next token
 */
void mem_token(int tok);

/**
 * \fn trash_all(int com)
 * \brief delete all the remaning to read
 *
 * \param com delete the next character until the next '\n'
 */
void trash_all(int com);

#endif /* !READ_CHAR_H */
