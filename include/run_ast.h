#ifndef RUN_AST_H
# define RUN_AST_H

/**
 * \file run_ast.c
 *
 * Execute the builded AST
 */

/**
 * \fn run_cmd(struct astcmd core, int *state, int loops, int force)
 * \brief execute a simple command
 *
 * \param core data for the execution of the command
 * \param state state of the execution
 * \param loops number of loops of depth
 * \param force force the excution of execvp over builtin and function
 *
 * \return return value of the command
 */
int run_cmd(struct astcmd core, int *state, int loops, int force);

/**
 * \fn run_ast(struct ast_shell *ast, int *state, int loops)
 * \brief execute the AST
 *
 * \param ast AST to execute
 * \param state state of the execution
 * \param loops number of loops of depth
 *
 * \return return value of the ast
 */
int run_ast(struct ast_shell *ast, int *state, int loops);

#endif /* !RUN_AST_H */
