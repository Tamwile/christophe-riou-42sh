#ifndef SHELL42_H
# define SHELL42_H

/**
 * \file shell42.c
 *
 * Lauching file of the 42sh project.
 * Parse the options of the binary.
 */

/**
 * \struct info
 */
struct info
{
  char norc; /*!< 1 if --norc is specified, 0 otherwise */
  char ast_print; /*!< 1 if --ast-print is specified, 0 otherwise */
  char version; /*!< 1 if --version is specified, 0 otherwise */
  struct var_list *v_list; /*!< list of variable */
  struct func_list *func; /*!< list of all functions */
  struct ast_shell *ast; /*!< ast_shell to execute */
  int src; /*!< type of input */
  int stop; /*!< is the execution be stopped */
  int depth; /*!< function depth */
};

#endif /* !SHELL42_H */
