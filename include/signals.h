#ifndef SIGNALS_H
# define SIGNALS_H

/**
 * \file signal.c
 *
 * Manage signals
 */

/**
 * \fn signal_listenning(void)
 * \brief catch signal from the kernel
 */
void signal_listenning(void);

#endif /* !SIGNALS_H */
