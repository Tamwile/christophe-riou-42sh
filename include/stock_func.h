#ifndef STOCK_FUNC_H
# define STOCK_FUNC_H

/**
 * \file stock_func.c
 *
 * Manage the storage of the functions
 */

/**
 * \struct func_list
 */
struct func_list
{
  char *name; /*!< name of the function */
  struct ast_shell *func; /*!< AST of the function */
  struct func_list *next; /*!< next element of the list */
};

/**
 * \fn which_func(char *func)
 * \brief return the AST of the function 'func'
 *
 * \param func name of the function wanted
 *
 * \return AST of the function, NULL of it doesn't exist
 */
struct ast_shell *which_func(char *func);

/**
 * \fn add_func(char *func, struct ast_shell *body)
 * \brief store a function
 *
 * \param func name of the function
 * \param body AST of the function
 */
void add_func(char *func, struct ast_shell *body);

/**
 * \fn free_func(struct func_list *head)
 * \brief free all function
 *
 * \param head the head of the list of function
 */
void free_func(struct func_list *head);

#endif /* !STOCK_FUNC_H */
