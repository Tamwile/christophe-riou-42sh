#ifndef STR_H
# define STR_H

/**
 * \file str.c
 *
 * Manage string
 */

/**
 * \fn save_str(char *s)
 * \brief save the string on the heap
 *
 * \param s string to save
 * \return pointer to the string on the heap
 */
char *save_str(char *s);

/**
 * \fn uint_to_str(unsigned nb)
 * \brief cast unsigned to string
 *
 * \param nb number to cast
 * \return string represanting the unsigned nb
 */
char *uint_to_str(unsigned nb);

/**
 * \fn is_equality(const char *s, int var)
 * \brief is s an assignation
 *
 * \param s the string to test
 * \param var should the first part of the assignation be a valid variable
 *
 * \return 0 if there is no problem
 */
int is_equality(const char *s, int var);

/**
 * \fn cut_equality(char *s)
 * \brief fill a struct astassig by parsing 's'
 *
 * \param s the string to parse
 *
 * \return a valid struct astassig
 */
struct astassig cut_equality(char *s);

/**
 * \fn is_func(void)
 * \brief is the next token a valid function
 *
 * \return 1 is it is valid, 0 otherwize
 */
int is_func(void);

#endif /* !STR_H */
