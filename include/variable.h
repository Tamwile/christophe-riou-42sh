#ifndef VARIABLE_H
# define VARIABLE_H

/**
 * \file variable.c
 *
 * Variables' table
 */

/**
 * \enum var_type
 */
enum var_type
{
  VARIABLE = 1,
  ALIAS = 2
};

/**
 * \struct var_list
 */
struct var_list
{
  char *etiquette; /*!< name of the variable */
  char *value; /*!< value of the variable to expand */
  enum var_type type; /*!< type of variable (variable, alias) */
  int exported; /*!< 0: variable non exported; exported if gt 0 */
  struct var_list *next; /*!< pointer to next variable in variables' table */
};

/**
 * \fn add_var(struct var_list *v_list, char *etiquette, char *value,
               enum var_type type)
 * \brief function to add a new variable in the variables' table
 *
 * \param etiquette name of the variable
 * \param value value of the variable to expand
 * \param type type of variable (variable, alias)
 * \param export is the variable exported
 * \return pointer to the new variables' table
 */
void add_var(char *etiquette, char *value, enum var_type type, int export);

/**
 * \fn get_value(struct var_list *v_list, char *etiquette, enum var_type type)
 * \brief function to find the value of a variable in the variables' table
 *
 * \param etiquette name of the variable
 * \param type type of variable (variable, alias)
 * \return value of the variable into string
 */
char *get_value(char *etiquette, enum var_type type);

/**
 * \fn free_var_list(struct var_list *v_list)
 * \brief free all the variables' table
 *
 * \param v_list pointer on the head of the variables' table
 */
void free_var_list(struct var_list *v_list);

struct var_list *pop_alias(struct var_list *list, char *alias);

struct var_list *unalias_all(struct var_list *list);

#endif /* !VARIABLE_H */
