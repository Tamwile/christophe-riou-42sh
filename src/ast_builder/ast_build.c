# include <err.h>
# include <stdio.h>
# include <stdlib.h>

# include "function.h"
# include "astfree.h"
# include "ast_shell.h"
# include "error.h"
# include "astloop.h"
# include "asttest.h"
# include "shell42.h"
# include "variable.h"
# include "lexer.h"
# include "str.h"
# include "expand.h"
# include "read_char.h"
# include "ast_build.h"

extern struct source g_src;
extern struct info g_info;

/**
 * \fn init_ast_shell(int tag, unsigned neg)
 * \brief initialise a struct ast_shell
 *
 * \param tag the type of the ast_shell (A_FOR, A_IF ... )
 * \param neg the number of negation in front of the command
 * \return the initialized struct
 */
struct ast_shell *init_ast_shell(int tag, unsigned neg)
{
  struct ast_shell *out = calloc(sizeof (struct ast_shell), 1);
  if (!out)
    err(1, "failed allocation");

  out->neg = neg % 2;
  out->tag = tag;
  return out;
}

/**
 * \fn build_ast_assig(char *word, unsigned neg)
 * \brief build AST for 'var=value'
 *
 * \param word the string 'var=value' not parsed
 * \param neg the number of negation in front of the command
 * \return a valid struct ast_shell, NULL otherwize
 */
static struct ast_shell *build_ast_assig(char *word, unsigned neg)
{
  struct astassig core = cut_equality(word);
  free(word);

  struct ast_shell *out = init_ast_shell(A_ASSIG, neg);
  out->core.astassig = core;
  return out;
}

/**
 * \fn build_ast_delim(unsigned neg, int tok_open)
 * \brief build AST for '(' ... ')' or '{' ... '}'
 *
 * \param neg the number of negation in front of the command
 * \param tok_open TOK_OPEN or TOK_BRACKOP
 * \return a valid struct ast_shell, NULL otherwize
 */
static struct ast_shell *build_ast_delim(unsigned neg, int tok_open)
{
  struct astdelim core;

  core.body = build_ast_shell(0, 0);
  if (unexpected(core.body->tag, -1, -1))
  {
    free_ast_shell(core.body, 0);
    return NULL;
  }

  int expect = (tok_open == TOK_OPEN) ? A_CLOSE : A_BRACKCL;
  if (build_loop(core.body, expect, -1, NULL))
  {
    free_ast_shell(core.body, 0);
    return NULL;
  }

  struct ast_shell *out = init_ast_shell(A_DELIM, neg);
  out->core.astdelim = core;
  return out;
}

/**
 * \fn choose_ast_shell2(int tok, unsigned neg)
 * \brief the continuation of choose_ast_shell()
 *
 * \param tok the token read
 * \param neg the number of negation in front of the command
 * \return the valid struct according to the token tok, NULL otherwize
 */
static struct ast_shell *choose_ast_shell2(int tok, unsigned neg)
{
  if (neg && (tok == TOK_ELSE || tok == TOK_DONE || tok == TOK_FI ||
              tok == TOK_THEN || tok == TOK_ELIF || tok == TOK_ESUALC ||
              tok == TOK_ESAC || tok == TOK_DO))
  {
    print_err_tok(tok);
    return NULL;
  }

  switch (tok)
  {
  case TOK_DONE:
    return init_ast_shell(A_DONE, 0);
  case TOK_DO:
    return init_ast_shell(A_DO, 0);
  case TOK_ELSE:
    return init_ast_shell(A_ELSE, 0);
  case TOK_FI:
    return init_ast_shell(A_FI, 0);
  case TOK_THEN:
    return init_ast_shell(A_THEN, 0);
  case TOK_ELIF:
    return init_ast_shell(A_ELIF, 0);
  case TOK_FUNC:
    return build_ast_function(neg, NULL);
  case TOK_CLOSE:
    return init_ast_shell(A_CLOSE, 0);
  case TOK_BRACKCL:
    return init_ast_shell(A_BRACKCL, 0);
  case TOK_ESUALC:
    return init_ast_shell(A_ESUALC, 0);
  case TOK_ESAC:
    return init_ast_shell(A_ESAC, 0);
  case TOK_LINE:
    return init_ast_shell(A_LINE, neg);
  case TOK_VOID:
    return init_ast_shell(A_LINE, neg);
  default:
    print_err_tok(tok);
    return NULL;
  }
}

/**
 * \fn choose_ast_shell(int tok, char *word)
 * \brief select the good function to call according to the token
 *
 * \param tok the token read
 * \param word the value of the word if tok is a TOK_WORD or TOK_ASSIG
 * \return the valid struct according to the token tok, NULL otherwize
 */
static struct ast_shell *choose_ast_shell(int tok, char *word)
{
  unsigned neg = 0;
  while (tok == TOK_NEG)
  {
    neg++;
    tok = get_next_token(&word, 0, 0);
  }

  switch (tok)
  {
  case TOK_WORD:
    ;
    unsigned nb = 0;
    char **cmd = get_full_word(word, &nb);
    struct astcmd core;
    core.cmd = cmd;
    core.argc = nb;
    struct ast_shell *a_cmd = init_ast_shell(A_CMD, neg);
    a_cmd->core.astcmd = core;
    return a_cmd;
  case TOK_ASSIG:
    return build_ast_assig(word, neg);
  case TOK_FOR:
    return build_ast_for(neg);
  case TOK_WHILE:
    return build_ast_while(neg, 0);
  case TOK_UNTIL:
    return build_ast_while(neg, 1);
  case TOK_CASE:
    return build_ast_case(neg);
  case TOK_IF:
    return build_ast_if(neg);
  case TOK_FUNC:
    return build_ast_function(neg, NULL);
  case TOK_HIDE_FUNC:
    return build_ast_function(neg, word);
  case TOK_OPEN:
    return build_ast_delim(neg, tok);
  case TOK_BRACKOP:
    return build_ast_delim(neg, tok);
  default:
    return choose_ast_shell2(tok, neg);
  }
}

/**
 * \fn build_ast_connector(int tok, struct ast_shell *ast_tmp)
 * \brief build the struct astope for handling of '&&' and '||'
 *
 * \param tok TOK_AND or TOK_OR
 * \param ast_tmp last ast_shell builded
 * \return a valid struct ast_shell for an operator, NULL otherwize
 */
static struct ast_shell *build_ast_connector(int tok, struct ast_shell *ast_tmp)
{
  struct astope core;
  core.tag = tok;
  core.left_part = ast_tmp;
  core.right_part = build_ast_shell(2, 1);
  if (!core.right_part)
  {
    free_ast_shell(core.left_part, 0);
    return NULL;
  }

  struct ast_shell *a_ope = init_ast_shell(A_OPE, 0);
  a_ope->core.astope = core;

  int first = 1;
  struct ast_shell *out = a_ope;
  struct ast_shell *last = NULL;
  while (a_ope->core.astope.right_part->tag == A_OPE)
  {
    struct ast_shell *tmp = a_ope->core.astope.right_part;
    a_ope->core.astope.right_part = tmp->core.astope.left_part;
    tmp->core.astope.left_part = a_ope;

    if (first)
    {
      first = 0;
      out = tmp;
    }
    else
      last->core.astope.left_part = tmp;
    last = tmp;
  }
  return out;
}

/**
 * \fn build_list(struct ast_shell *ast_shell, int tok, int first)
 * \brief build the possible consecitive list of commandes
 *
 * \param ast_shell the head of the list
 * \param tok tehe next token parsed
 * \param first is it the main call of this function
 * \return the list of consecutive commands
 */
static struct ast_shell *build_list(struct ast_shell *ast_shell, int tok,
                                    int first)
{
  char *word = NULL;
  struct ast_shell *ast_tmp = ast_shell;
  while ((g_src.ahead != '\n' && tok != TOK_LINE &&
         (tok != TOK_SEMIC || first == 1)) && ast_tmp && g_src.ahead)
  {
    if (tok == TOK_AND || tok == TOK_OR)
    {
      ast_shell = build_ast_connector(tok, ast_shell);
      if (!ast_shell)
        return ast_shell;
      tok = get_next_token(NULL, !first, 0);
      if (tok == TOK_AND || tok == TOK_OR)
        continue;
      if (tok != TOK_SEMIC)
        mem_token(tok);
      ast_tmp = ast_shell;
    }
    tok = get_next_token(&word, !first, 0);

    if (tok == TOK_DO || tok == TOK_THEN || tok == TOK_FI || tok == TOK_ELSE ||
        tok == TOK_DONE || tok == TOK_ESUALC || tok == TOK_CLOSE ||
        tok == TOK_ELIF || tok == TOK_ESAC)
    {
      mem_token(tok);
      return ast_shell;
    }
    if (tok == TOK_VOID || tok == TOK_SPACE || (first && tok == TOK_SEMIC))
      continue;
    if (tok == TOK_LINE || (tok == TOK_SEMIC && first != 1))
      break;

    while (ast_tmp->next)
      ast_tmp = ast_tmp->next;
    ast_tmp->next = choose_ast_shell(tok, word);
    ast_tmp = ast_tmp->next;
    if (!ast_tmp)
    {
      free_ast_shell(ast_shell, 0);
      return NULL;
    }

    if (!g_src.ahead)
      break;
    tok = get_next_token(&word, !first, 0);
  }

  if (first && g_src.ahead == '\n')
    tok = get_next_token(NULL, !first, 0);

  return ast_shell;
}

struct ast_shell *build_ast_shell(int first, int onlyone)
{
  if (!g_src.ahead && g_src.mem_tok == TOK_VOID && g_src.tag != INTERACTIVE)
    return NULL;

  char *word = NULL;
  int tok = -10;
  do {
    if (g_src.tag == INTERACTIVE && first == 1 && tok != TOK_SEMIC)
    {
      char *ps1 = get_value("PS1", VARIABLE);
      printf(ps1);
      free(ps1);
    }
    tok = get_next_token(&word, !first, 0);
    while (g_src.ahead && (tok == TOK_SPACE || tok == TOK_VOID))
      tok = get_next_token(&word, !first, 0);
  } while ((tok == TOK_LINE) && (g_src.ahead || g_src.tag == INTERACTIVE));
  struct ast_shell *ast_shell = choose_ast_shell(tok, word);

  if (!ast_shell)
    return NULL;

  int tag = ast_shell->tag;
  if (tag == A_DONE || tag == A_ELSE || tag == A_FI || tag == A_CLOSE ||
      tag == A_BRACKCL || tag == A_THEN || tag == A_ELIF || onlyone ||
      (g_src.ahead == '\n' && !first) || !g_src.ahead || tag == A_DO ||
      tok == TOK_SEMIC || tag == A_ESUALC || tag == A_ESAC)
    return ast_shell;

  tok = get_next_token(NULL, !first, 0);
  if (tok == TOK_WORD || tok == TOK_ASSIG || tok == TOK_HIDE_FUNC)
  {
    print_err_tok(tok);
    free_ast_shell(ast_shell, 0);
    return NULL;
  }
  if (tok != TOK_LINE && tok != TOK_AND && tok != TOK_OR && tok != TOK_SEMIC)
    mem_token(tok);

  return build_list(ast_shell, tok, first);
}
