# include <stdlib.h>

# include "ast_shell.h"
# include "astfree.h"

void free_array(char **s, int nb)
{
  if (nb == -1)
  {
    char **tmp = s;
    for (; *tmp; tmp++)
      free(*tmp);
    free(s);
    return;
  }
  for (int ite = 0; ite < nb; ite++)
    free(s[ite]);
  free(s);
}

void free_case(struct clause *list, int runtime)
{
  while (list)
  {
    struct clause *tmp = list->next;

    for (unsigned ite = 0; *(list->test + ite); ite++)
      free(*(list->test + ite));
    free(list->test);
    free_ast_shell(list->body, runtime);
    free(list);

    list = tmp;
  }
}

static void free_choose2(struct ast_shell *ast, int runtime)
{
  switch (ast->tag)
  {
  case A_IF:
    free_ast_shell(((ast->core).astif).test, runtime);
    free_ast_shell(((ast->core).astif).then_body, runtime);
    free_ast_shell(((ast->core).astif).else_body, runtime);
    break;
  case A_OPE:
    free_ast_shell(((ast->core).astope).left_part, runtime);
    free_ast_shell(((ast->core).astope).right_part, runtime);
    break;
  case A_DELIM:
    free_ast_shell(((ast->core).astdelim).body, runtime);
    break;
  case A_CASE:
    free_case(ast->core.astcase.head, runtime);
    free(ast->core.astcase.word);
    break;
  case A_ASSIG:
    free(ast->core.astassig.var);
    free(ast->core.astassig.value);
    break;
  }
}

static void free_choose(struct ast_shell *ast, int runtime)
{
  switch (ast->tag)
  {
  case A_CMD:
    for (unsigned ite = 0; ite < ast->core.astcmd.argc; ite++)
      free(ast->core.astcmd.cmd[ite]);
    free(ast->core.astcmd.cmd);
    break;
  case A_FOR:
    for (unsigned ite = 0; ite < ast->core.astfor.nb_in; ite++)
      free(ast->core.astfor.in[ite]);
    free(ast->core.astfor.in);
    free(ast->core.astfor.var);
    free_ast_shell(((ast->core).astfor).body, runtime);
    break;
  case A_WHILE:
    free_ast_shell(((ast->core).astwhile).test, runtime);
    free_ast_shell(((ast->core).astwhile).body, runtime);
    break;
  case A_FUNC:
    if (runtime)
      break;
    free(((ast->core).astfunc).name);
    free_ast_shell(((ast->core).astfunc).body, runtime);
    break;
  default:
    free_choose2(ast, runtime);
  }
}

void free_ast_shell(struct ast_shell *ast, int runtime)
{
  if (!ast)
    return;

  do {
    free_choose(ast, runtime);
    struct ast_shell *tmp = ast;
    ast = ast->next;
    free(tmp);
  } while (ast);
}
