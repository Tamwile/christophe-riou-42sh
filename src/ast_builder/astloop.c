# include <err.h>
# include <stdio.h>
# include <stdlib.h>

# include "ast_shell.h"
# include "astfree.h"
# include "error.h"
# include "ast_build.h"
# include "lexer.h"
# include "read_char.h"
# include "astloop.h"

extern struct source g_src;

int build_loop(struct ast_shell *tmp, int stop1, int stop2, int *tag)
{
  if (!tmp)
    return 1;
  while (tmp && tmp->next)
    tmp = tmp->next;
  int outtag = tmp->tag;

  while (tmp->tag != stop1 && tmp->tag != stop2)
  {
    struct ast_shell *new = build_ast_shell(0, 0);
    while (new && new->next)
      new = new->next;
    if (!new || unexpected(new->tag, stop1, stop2))
    {
      free_ast_shell(new, 0);
      return 1;
    }
    if (new->tag == stop1 || new->tag == stop2)
    {
      outtag = new->tag;
      free(new);
      break;
    }

    while (tmp->next)
      tmp = tmp->next;

    tmp->next = new;
    tmp = new;
  }
  tmp->next = NULL;

  if (tag)
    *tag = outtag;
  return 0;
}

struct ast_shell *build_ast_while(unsigned neg, int type)
{
  struct astwhile core;

  core.test = build_ast_shell(0, 0);
  if (unexpected(core.test->tag, -1, -1) ||
      build_loop(core.test, A_DO, -1, NULL))
  {
    free_ast_shell(core.test, 0);
    return NULL;
  }
  struct ast_shell *tmp = core.test;
  while (tmp->next)
    tmp = tmp->next;
  tmp->neg = (tmp->neg + type) % 2;

  core.body = build_ast_shell(0, 0);
  if (unexpected(core.body->tag, -1, -1) ||
      build_loop(core.body, A_DONE, -1, NULL))
  {
    free_ast_shell(core.test, 0);
    free_ast_shell(core.body, 0);
    return NULL;
  }

  struct ast_shell *a_while = init_ast_shell(A_WHILE, neg);
  a_while->core.astwhile = core;
  return a_while;
}

/**
 * \fn build_astfor(struct astfor core, unsigned neg)
 * \brief continuation of build_ast_for()
 *
 * \param neg the number of negation in front of the command
 * \param core the begining of the construction of the core of the ast_shell
 * \return a valid struct ast_shell, NULL otherwize
 */
static struct ast_shell *build_astfor(struct astfor core, unsigned neg)
{
  int tok = get_next_token(NULL, 1, 0);
  if (tok != TOK_SEMIC && tok != TOK_LINE)
  {
    print_err_tok(tok);
    free(core.var);
    free_array(core.in, core.nb_in);
    return NULL;
  }

  do {
    tok = get_next_token(NULL, 1, 0);
  } while (tok == TOK_LINE);

  if (tok != TOK_DO)
  {
    print_err_tok(tok);
    free(core.var);
    free_array(core.in, core.nb_in);
    return NULL;
  }

  core.body = build_ast_shell(0, 0);
  if (build_loop(core.body, A_DONE, -1, NULL) ||
      unexpected(core.body->tag, -1, -1))
  {
    free(core.var);
    free_array(core.in, core.nb_in);
    free_ast_shell(core.body, 0);
    return NULL;
  }

  struct ast_shell *a_for = init_ast_shell(A_FOR, neg);
  a_for->core.astfor = core;
  return a_for;
}

struct ast_shell *build_ast_for(unsigned neg)
{
  if (g_src.ahead == '\n' || g_src.ahead =='\0')
  {
    print_err_tok(TOK_LINE);
    return NULL;
  }
  struct astfor core;
  core.var = get_next_var(1);
  if (!core.var)
    return NULL;

  int tok = get_next_token(NULL, 1, 0);
  while (tok == TOK_LINE)
    tok = get_next_token(NULL, 1, 0);

  if (tok != TOK_IN)
  {
    print_err_tok(tok);
    free(core.var);
    return NULL;
  }

  char *val;
  unsigned nb = 0;
  get_next_token(&val, 1, 1);
  core.in = get_full_word(val, &nb);
  core.nb_in = nb;
  return build_astfor(core, neg);
}
