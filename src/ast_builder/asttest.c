# include <err.h>
# include <stdio.h>
# include <stdlib.h>

# include "ast_shell.h"
# include "ast_build.h"
# include "lexer.h"
# include "error.h"
# include "read_char.h"
# include "astfree.h"
# include "astloop.h"
# include "asttest.h"

extern struct source g_src;

/**
 * \fn build_loop_if(struct ast_shell *tmp, int *tag)
 * \brief add all the consecutive command to the next of tmp
 *
 * \param tmp the head of the list
 * \param tag the type of ast_shell which ended the list
 * \return !=0 if an error occured, 0 otherwize
 */
static int build_loop_if(struct ast_shell *tmp, int *tag)
{
  while (tmp && tmp->next)
    tmp = tmp->next;
  int outtag = tmp->tag;

  while (tmp->tag != A_FI && tmp->tag != A_ELSE && tmp->tag != A_ELIF)
  {
    struct ast_shell *new = build_ast_shell(0, 0);
    while (new && new->next)
      new = new->next;
    if (!new || (new->tag != A_FI && unexpected(new->tag, A_ELSE, A_ELIF)))
      return 1;
    if (new->tag == A_FI || new->tag == A_ELSE || new->tag == A_ELIF)
    {
      outtag = new->tag;
      free(new);
      break;
    }

    while (tmp->next)
      tmp = tmp->next;

    tmp->next = new;
    tmp = new;
  }
  tmp->next = NULL;

  if (tag)
    *tag = outtag;
  return 0;
}

/**
 * \fn build_astif(struct ast_shell *a_if, struct astif core, int tag)
 * \brief continuation of build_ast_if()
 *
 * \param a_if the begining of the construction of the ast_shell to return
 * \param core the begining of the construction of the core of the ast_shell
 * \param tag the last type of 'A_*' which ended the 'then' part
 * \return a valid struct ast_shell, NULL otherwize
 */
static struct ast_shell *build_astif(struct ast_shell *a_if, struct astif core,
                                     int tag)
{
  if (tag == A_FI)
    return a_if;
  if (tag == A_ELIF)
  {
    core.else_body = build_ast_if(0);
    if (!core.else_body)
    {
      free_ast_shell(core.test, 0);
      free_ast_shell(core.then_body, 0);
      return NULL;
    }
    a_if->core.astif = core;
    return a_if;
  }
  if (unexpected(tag, A_ELSE, -1))
  {
    free_ast_shell(core.test, 0);
    free_ast_shell(core.then_body, 0);
    return NULL;
  }

  core.else_body = build_ast_shell(0, 0);
  if (unexpected(core.else_body->tag, -1, -1) ||
      build_loop(core.else_body, A_FI, -1, NULL))
  {
    free_ast_shell(core.test, 0);
    free_ast_shell(core.then_body, 0);
    free_ast_shell(core.else_body, 0);
    return NULL;
  }

  a_if->core.astif = core;
  return a_if;
}

struct ast_shell *build_ast_if(unsigned neg)
{
  struct astif core;
  core.test = build_ast_shell(0, 0);
  if (build_loop(core.test, A_THEN, -1, NULL) ||
      unexpected(core.test->tag, -1, -1))
  {
    free_ast_shell(core.test, 0);
    return NULL;
  }

  int tag = 0;
  core.then_body = build_ast_shell(0, 0);
  if (build_loop_if(core.then_body, &tag) ||
      unexpected(core.then_body->tag, -1, -1))
  {
    free_ast_shell(core.test, 0);
    free_ast_shell(core.then_body, 0);
    return NULL;
  }

  core.else_body = NULL;
  struct ast_shell *a_if = init_ast_shell(A_IF, neg);
  a_if->core.astif = core;

  return build_astif(a_if, core, tag);
}

/**
 * \fn get_next_test(int first)
 * \brief parse the next WORD
 *
 * \param first is it the first call of this function ?
 * \return the next WORD
 */
static char *get_next_test(int first)
{
  char *val = NULL;
  int mem = (g_src.mem_tok != TOK_VOID);
  int tok = get_next_token(&val, 1, 1);

  while (tok == TOK_VOID)
  {
    free(val);
    val = NULL;
    mem = (g_src.mem_tok != TOK_VOID);
    tok = get_next_token(&val, 1, 1);
  }

  if (first && tok == TOK_OPEN)
  {
    free(val);
    val = NULL;
    mem = (g_src.mem_tok != TOK_VOID);
    tok = get_next_token(&val, 1, 1);
  }

  if (tok == TOK_ESAC)
  {
    if (!mem)
      free(val);
    return NULL;
  }

  return val;
}

/**
 * \fn build_case_test(void)
 * \brief build the next consecutive array of WORD separated by a '|'
 *
 * \return array of WORDS
 */
static char **build_case_test(void)
{
  char *test = get_next_test(1);
  if (!test)
    return NULL;

  char **out = calloc(sizeof (char *), 2);
  if (!out)
    err(1, "Failed allocation");
  *out = test;

  int tok = get_next_token(NULL, 1, 0);
  unsigned nb = 1;

  while (tok == TOK_VOID)
    tok = get_next_token(NULL, 1, 0);

  while (tok == TOK_PIPE)
  {
    char *test = get_next_test(0);

    out[nb] = test;
    nb++;
    out = realloc(out, sizeof (char *) * (nb + 1));
    out[nb] = NULL;
    tok = get_next_token(NULL, 1, 0);
  }

  if (tok != TOK_CLOSE)
  {
    print_err_tok(tok);
    free_array(out, -1);
    return NULL;
  }

  return out;
}

/**
 * \fn build_next_clause(void)
 * \brief parse the next clause of the case
 *
 * \return a valid struct clause, NULL otherwize
 */
static struct clause *build_next_clause(void)
{
  char **test = build_case_test();
  if (!test)
    return NULL;

  struct ast_shell *body = build_ast_shell(0, 0);
  int tag;
  if (build_loop(body, A_ESUALC, A_ESAC, &tag) || unexpected(body->tag, -1, -1))
  {
    free_array(test, -1);
    free_ast_shell(body, 0);
    return NULL;
  }
  if (tag == A_ESAC)
    mem_token(TOK_ESAC);

  struct clause *out = calloc(sizeof (struct clause), 1);
  if (!out)
    err(1, "Failed allocation");

  out->body = body;
  out->test = test;
  return out;
}

struct ast_shell *build_ast_case(unsigned neg)
{
  struct astcase core;
  char *val;
  int tok = get_next_token(&val, 1, 1);
  core.word = val;

  tok = get_next_token(NULL, 1, 0);
  if (tok != TOK_IN)
  {
    print_err_tok(tok);
    free(core.word);
    return NULL;
  }

  struct clause *cls = build_next_clause();
  if (!cls)
  {
    free(core.word);
    return NULL;
  }
  core.head = cls;

  do {
    struct clause *tmp = build_next_clause();
    if (g_src.error)
    {
      free(core.word);
      free_case(core.head, 0);
      return NULL; //free
    }
    cls->next = tmp;
    cls = tmp;
  } while (cls);

  struct ast_shell *a_case = init_ast_shell(A_CASE, neg);
  a_case->core.astcase = core;
  return a_case;
}
