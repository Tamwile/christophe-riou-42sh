# include <stdio.h>
# include <stdlib.h>

# include "ast_shell.h"
# include "lexer.h"
# include "error.h"
# include "ast_build.h"
# include "astfree.h"
# include "read_char.h"
# include "function.h"

extern struct source g_src;

struct ast_shell *build_ast_function(unsigned neg, char *name)
{
  if (!name && (g_src.ahead == '\n' || !g_src.ahead))
  {
    print_err_tok(TOK_LINE);
    return NULL;
  }

  int tok;
  if (!name)
    tok = get_next_token(&name, 0, 1);

  char *val;
  tok = get_next_token(&val, 0, 0);
  if (tok != TOK_PARENTH)
  {
    print_err_tok(tok);
    free(name);
    return NULL;
  }

  struct astfunc core;
  core.name = name;
  core.body = build_ast_shell(0, 1);
  if (!core.body || unexpected(core.body->tag, -1, -1))
  {
    free(name);
    free_ast_shell(core.body, 0);
    return NULL;
  }
  tok = core.body->tag;
  if (tok != A_FOR && tok != A_IF && tok != A_WHILE && tok != A_DELIM &&
      tok != A_CASE)
  {
    free(name);
    free_ast_shell(core.body, 0);
    g_src.error = 1;
    if (g_src.tag == INTERACTIVE)
      g_src.error = 2;
    fprintf(stderr, "42sh: syntax error near token '()'\n");
    return NULL;
  }

  struct ast_shell *a_func = init_ast_shell(A_FUNC, neg);
  a_func->core.astfunc = core;

  return a_func;
}
