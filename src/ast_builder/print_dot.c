#include <stdio.h>

#include "ast_shell.h"
#include "lexer.h"
#include "print_dot.h"

static unsigned g_name = 0;

static void print_ast_shell(FILE *fp, struct ast_shell *ast_s);

static void print_ast_for(FILE *fp, struct astfor *ast_f)
{
  g_name += 1;
  int tmp = g_name;
  fprintf(fp, "%d [shape=record,label=\"{tag FOR | var = %s\\n", g_name,
          ast_f->var);
  fprintf(fp, "in \\n ");
  for (unsigned i = 0; i < ast_f->nb_in ; i++)
  {
    fprintf(fp, "%s \\n", *(ast_f->in+i));
  }

  fprintf(fp, "next = NULL}\"];\n");
  printf("astfor ast_shell body\n");
  printf("TAG : %d\n", ast_f->body->tag);
  print_ast_shell(fp, ast_f->body);
  fprintf(fp, "%d -> %d [label=\"do\"];\n", tmp, tmp+1);
  //fprintf(fp, "%d [shape=box,label=\"test\" ];", g_name);
  //if (ast_s->next)
    //fprintf(fp, "%d -> %d [label=\"next\"]\n", tmp, g_name+1);
}

static void print_ast_while(FILE *fp, struct astwhile *ast_w)
{
  g_name += 1;
  int tmp = g_name;
  fprintf(fp, "%d [shape=box,label=\"tag WHILE \"]; \n", g_name);
  print_ast_shell(fp, ast_w->test);
  print_ast_shell(fp, ast_w->body);
  fprintf(fp, "%d -> %d [label=\"test\"];\n", tmp, tmp+1);
  fprintf(fp, "%d -> %d [label=\"body\"];\n", tmp, tmp+2);
}

static void print_ast_if(FILE *fp, struct astif *ast_i)
{
  g_name+=1;
  int tmp = g_name;
  fp = fp;
  fprintf(fp, "%d [shape=record,label=\"tag IF\"]; \n", g_name);
  print_ast_shell(fp, ast_i->test);
  print_ast_shell(fp, ast_i->then_body);
  print_ast_shell(fp, ast_i->else_body);
  fprintf(fp, "%d -> %d [label=\"test\"];\n", tmp, tmp+1);
  fprintf(fp, "%d -> %d [label=\"then\"];\n", tmp, tmp+2);
  fprintf(fp, "%d -> %d [label=\"else\"];\n", tmp, tmp+3);
}

static void print_ast_cmd(FILE *fp, struct astcmd *ast_cmd)
{
  g_name += 1;
  fprintf(fp, "%d [shape=record,label=\"{tag CMD | argc = %u\\n ", g_name,
          ast_cmd->argc);
  for (unsigned i = 0; i < ast_cmd->argc ; i++)
  {
    fprintf(fp, "arg = %s \\n", *(ast_cmd->cmd+i));
  }
  //fprintf(fp, " \\nredirection = list1");
  fprintf(fp, " }\"];\n");
}

static void print_ast_ope(FILE *fp, struct astope *ast_op)
{
  g_name += 1;
  int tmp = g_name;
  if (ast_op->tag == TOK_AND)
    fprintf(fp, "%d [shape=record,label=\"{tag OPERATOR | AND}\"];\n ", g_name);
  else if (ast_op->tag == TOK_OR)
    fprintf(fp, "%d [shape=record,label=\"{tag OPERATOR | OR}\"];\n ", g_name);
  print_ast_shell(fp, ast_op->left_part);
  fprintf(fp, "%d -> %d [label=\"left\"];\n", tmp, tmp+1);
  print_ast_shell(fp, ast_op->right_part);
  fprintf(fp, "%d -> %d [label=\"right\"];\n", tmp, g_name);
}

static void print_ast_pipe(FILE *fp, struct astpipe *ast_pipe)
{
  g_name += 1;
  int tmp = g_name;
    fprintf(fp, "%d [shape=record,label=\"{tag PIPE}\"];\n ", g_name);
  print_ast_shell(fp, ast_pipe->left_part);
  fprintf(fp, "%d -> %d [label=\"left\"];\n", tmp, tmp+1);
  print_ast_shell(fp, ast_pipe->right_part);
  fprintf(fp, "%d -> %d [label=\"right\"];\n", tmp, g_name);
}

static void print_ast_delim(FILE *fp, struct astdelim *ast_delim)
{
  g_name += 1;
  int tmp = g_name;
  fprintf(fp, "%d [shape=record,label=\"{tag DELIM}\"];\n ", g_name);
  print_ast_shell(fp, ast_delim->body);
  fprintf(fp, "%d -> %d [label=\"parenthesis\"];\n", tmp, tmp+1);
}

static void print_ast_func(FILE *fp, struct astfunc *ast_func)
{
    g_name += 1;
  int tmp = g_name;
  fprintf(fp, "%d [shape=record,label=\"{tag FUNCTION | %s}\"];\n ", g_name,
          ast_func->name);
  print_ast_shell(fp, ast_func->body);
  fprintf(fp, "%d -> %d [label=\"right\"];\n", tmp, tmp+1);
}

static void print_ast_shell(FILE *fp, struct ast_shell *ast_s)
{
  while (ast_s)
  {
    int tmp = g_name+1;
    printf("tag = %d\n", ast_s->tag);
    switch(ast_s->tag)
    {
    case A_FOR:
      print_ast_for(fp, &ast_s->core.astfor);
      break;

    case A_WHILE:
      print_ast_while(fp, &ast_s->core.astwhile);
      break;

    case A_IF:
      print_ast_if(fp, &ast_s->core.astif);
      break;

    case A_CMD:
      print_ast_cmd(fp, &ast_s->core.astcmd);
      break;

    case A_OPE:
      print_ast_ope(fp, &ast_s->core.astope);
      break;

    case A_PIPE:
      print_ast_pipe(fp, &ast_s->core.astpipe);
      break;
    case A_DELIM:
      print_ast_delim(fp, &ast_s->core.astdelim);
      break;
    case A_FUNC:
      print_ast_func(fp, &ast_s->core.astfunc);
      break;
    }
    if (ast_s->next)
    {
      fprintf(fp, "%d -> %d [label=\"next\"]\n", tmp, g_name+1);
    }
    ast_s = ast_s->next;
  }
}


void print_dot(struct ast_shell *ast_s)
{
  FILE *fp;
  if (g_name == 0)
    fp = fopen("graph.dot", "w+");
  else
    fp = fopen("graph.dot", "a+");
  if (!fp)
    printf("erreur ouverture fichier\n");
  fprintf(fp, "digraph AST_%d {\n", g_name);
  fprintf(fp, "size =\"2,2\";\n");
  fprintf(fp, "compound=true;\n");

  if (!ast_s)
    printf("AST NULL\n");
  else
  {
    print_ast_shell(fp, ast_s);
  }

  fprintf(fp, "}\n");
  fclose(fp);
}
