# include <ctype.h>
# include <string.h>
# include <stdio.h>
# include <stdlib.h>

# include "variable.h"
# include "ast_shell.h"
# include "shell42.h"
# include "builtin.h"
# include "blt_block.h"

extern struct info g_info;

static int is_int(char *s)
{
  if (!s || strlen(s) > 19)
    return 0;

  if (*s == '-')
    s++;

  if (!*s)
    return 0;

  for (; *s; s++)
  {
    if (!isdigit(*s))
      return 0;
  }
  return 1;
}

static int blt_exit(char **args, unsigned argc, int *state)
{
  if (!argc)
  {
    if (g_info.src == INTERACTIVE)
      printf("exit\n");
    *state = -1; // EXIT
    char *out = get_value("?", VARIABLE);
    int nb = atoi(out);
    free(out);
    return nb;
  }

  if (!is_int(*args))
  {
    if (g_info.src == INTERACTIVE)
      printf("exit\n");
    fprintf(stderr, "42sh: %s: numeric argument required\n", *args);
    *state = -1;
    return 2;
  }

  if (argc > 1)
  {
    fprintf(stderr, "42sh: exit: too many arguments\n");
    return 1;
  }

  if (g_info.src == INTERACTIVE)
    printf("exit\n");
  *state = -1;
  return atoi(*args);
}


static int blt_break(char **args, unsigned argc, int *state, int loops)
{
  if (!loops)
    return 0;

  if (!argc)
  {
    *state = 2;
    return 0;
  }

  if (!is_int(*args))
  {
    fprintf(stderr, "42sh: %s: numeric argument required\n", *args);
    *state = -2;
    return 128;
  }

  if (argc > 1)
  {
    fprintf(stderr, "42sh: break: too many arguments\n");
    *state = -2;
    return 1;
  }

  int out = atoi(*args);
  if (out <= 0)
  {
    fprintf(stderr, "42sh: %d: loop count out of range\n", out);
    *state = -2;
    return 1;
  }

  out = (out < loops) ? out : loops;
  *state = out * 2;
  return 0;
}

static int blt_continue(char **args, unsigned argc, int *state, int loops)
{
  if (!loops)
    return 0;

  if (!argc)
  {
    *state = 1;
    return 0;
  }

  if (!is_int(*args))
  {
    fprintf(stderr, "42sh: %s: numeric argument required\n", *args);
    *state = -2;
    return 128;
  }

  if (argc > 1)
  {
    fprintf(stderr, "42sh: continue: too many arguments\n");
    *state = -2;
    return 1;
  }

  int out = atoi(*args);
  if (out <= 0)
  {
    fprintf(stderr, "42sh: %d: loop count out of range\n", out);
    *state = -2;
    return 1;
  }

  out = (out < loops) ? out : loops;
  *state = out * 2 - 1;
  return 0;
}

int blt_block(char **args, unsigned argc, int *state, int loops)
{
  switch (get_builtin_id(*args))
  {
  case B_EXIT:
    return blt_exit(args + 1, argc, state);
  case B_BREAK:
    return blt_break(args + 1, argc, state, loops);
  case B_CONTINUE:
    return blt_continue(args + 1, argc, state, loops);
  }
  return 0;
}
