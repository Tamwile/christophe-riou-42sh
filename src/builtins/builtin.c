# include <stdio.h>
# include <stdlib.h>
# include <string.h>
# include <unistd.h>

# include "ast_build.h"
# include "ast_shell.h"
# include "blt_block.h"
# include "name.h"
# include "run_ast.h"
# include "shell42.h"
# include "str.h"
# include "variable.h"

# include "builtin.h"

extern struct info g_info;
extern char** environ;


static int blt_cd_home(char *pwd)
{
  char *home = get_value("HOME", VARIABLE);
  if (chdir(home) < 0)
  {
    fprintf(stderr, "42sh: HOME not set\n");
    free(home);
    free(pwd);
    return 1;
  }
  add_var("OLDPWD", pwd, VARIABLE, 1);
  add_var("PWD", home, VARIABLE, 1);
  free(home);
  free(pwd);
  return 0;
}

static int blt_cd_oldpwd(char *pwd)
{
  char *tmp = get_value("OLDPWD", VARIABLE);
  if (chdir(tmp) < 0)
  {
    fprintf(stderr, "42sh: OLDPWD not set\n");
    free(tmp);
    free(pwd);
    return 1;
  }
  add_var("OLDPWD", pwd, VARIABLE, 1);
  add_var("PWD", tmp, VARIABLE, 1);
  fprintf(stdout, "%s\n", tmp);
  free(tmp);
  free(pwd);
  return 0;
}

static int blt_cd(char **path, unsigned argc)
{
  char cwd[1024];
  char *pwd = get_value("PWD", VARIABLE);
  if (argc > 1)
  {
    fprintf(stderr, "42sh: cd: too many arguments\n");
    free(pwd);
    return 1;
  }
  else if (argc == 0)
    return blt_cd_home(pwd);
  else if (!strcmp(*path, "-"))
    return blt_cd_oldpwd(pwd);
  else
  {
    if (chdir(*path) < 0)
    {
      fprintf(stderr, "42sh: cd: %s: no such file or directory\n", *path);
      free(pwd);
      return 1;
    }
    char *curr_dir = getcwd(cwd, sizeof(cwd));
    if (curr_dir == NULL)
    {
      fprintf(stderr, "getcwd() error");
      free(pwd);
      return 1;
    }
    add_var("OLDPWD", pwd, VARIABLE, 1);
    add_var("PWD", curr_dir, VARIABLE, 1);
  }
  free(pwd);
  return 0;
}

static int blt_echo(char **argv, unsigned argc)
{
  unsigned i;
  if (argc != 0)
  {
    printf("%s", *(argv));
  }
  for (i = 1; i < argc; i++)
  {
    int tmp = printf(" %s", *(argv+i));
    if (tmp < 0)
      return -tmp;
  }
  printf("\n");
  return 0;
}

static int blt_alias(char **argv, unsigned argc)
{
  if (!argc)
    return 0;

  int out = 0;
  for (unsigned ite = 0; ite < argc; ite++)
  {
    int err = is_equality(*(argv + ite), 0);
    if (!err)
    {
      struct astassig assig = cut_equality(*(argv + ite));
      add_var(assig.var, assig.value, ALIAS, 0);
      free(assig.var);
      free(assig.value);
      continue;
    }
    if (err == -1)
    {
      fprintf(stderr, "42sh: alias: %s: invalid alias name\n", *(argv + ite));
      out = 1;
      continue;
    }
    char *alias = get_value(*(argv + ite), ALIAS);
    if (!alias)
    {
      fprintf(stderr, "42sh: alias: %s: not found\n", *(argv + ite));
      out = 1;
      continue;
    }
    printf("%s='%s'\n", *(argv + ite), alias);
  }

  return out;
}

static int blt_unalias(char **argv, unsigned argc)
{
  if (!argc)
  {
    fprintf(stderr, "unalias: usage: unalias [-a] name [name ...]\n");
    return 2;
  }

  if (!strcmp(*argv, "-a"))
  {
    g_info.v_list = unalias_all(g_info.v_list);
    return 0;
  }

  int out = 0;
  for (unsigned ite = 0; ite < argc; ite++)
  {
    if (!get_value(*(argv + ite), ALIAS))
    {
      fprintf(stderr, "42sh: unalias: %s: not found\n", *(argv + ite));
      out = 1;
      continue;
    }
    g_info.v_list = pop_alias(g_info.v_list, *(argv + ite));
  }

  return out;
}

static int blt_export(char **args, unsigned argc, int *state, int loops)
{
  state = state;
  loops = loops;
  if (argc == 0)
  {
    // print exported variables
    struct var_list *tmp = g_info.v_list;
    while (tmp)
    {
      if (tmp->type == VARIABLE && tmp->exported > 0)
      {
        if (tmp->value)
          printf("declare -x %s=\"%s\"\n", tmp->etiquette, tmp->value);
        else
          printf("declare -x %s\n", tmp->etiquette);
      }
      tmp = tmp->next;
    }
  }

  else
  {
    // assign vars and export them
    for (unsigned i = 0; i < argc; i++)
    {
      struct astassig out;
      if (! is_equality(args[i], 1))
      {
        out = cut_equality(args[i]);
        add_var(out.var, out.value, VARIABLE, 1);
        setenv(out.var, out.value, 1);

        free(out.var);
        free(out.value);
      }
      else
      {
        if (check_var(args[i]))
          add_var(args[i], NULL, VARIABLE, 1);
        else
        {
	  fprintf(stderr, "42sh: export: `%s': not a valid identifier\n",
                  args[i]);
          return 1;
        }
      }
    }
  }
  return 0;
}

int get_builtin_id(char *cmd)
{
  char tab[11][9] =
  {
    "exit",     "cd",
    "shopt",    "export",
    "alias",    "unalias",
    "echo",     "continue",
    "break",    "source",
    "history"
  };

  for (int ite = 0; ite < 11; ite++)
  {
    if (!strcmp(tab[ite], cmd))
      return ite;
  }
  return -1;
}

int exec_builtin(char **args, unsigned argc, int *state, int loops)
{
  switch (get_builtin_id(*args))
  {
  case B_CD:
    return blt_cd(args + 1, argc);
  case B_ECHO:
    return blt_echo(args + 1, argc);
  case B_ALIAS:
    return blt_alias(args + 1, argc);
  case B_UNALIAS:
    return blt_unalias(args + 1, argc);
  case B_EXPORT:
    return blt_export(args+1, argc, state, loops);
  default:
    return blt_block(args, argc, state, loops);
  }
  return 0;
}
