# include <fnmatch.h>
# include <stdio.h>

# include "ast_shell.h"
# include "variable.h"
# include "lexer.h"
# include "run_ast.h"
# include "looptest_run.h"

static int update_state(int *state)
{
  if (*state < 0)
    return -1;
  if (!*state)
    return 0;

  int out = *state % 2;
  *state = (*state - 2 < 0) ? 0 : *state - 2;

  return out * 2 - 1;
}

static int run_for(struct astfor core, int *state, int loops)
{
  int out = 0;
  for (unsigned ite = 0; ite < core.nb_in; ite++)
  {
    add_var(core.var, core.in[ite], VARIABLE, 0);
    out = run_ast(core.body, state, loops + 1);
    int choice = update_state(state);
    if (choice == -1)
      return out;
  }

  return out;
}

static int run_while(struct astwhile core, int *state, int loops)
{
  int out = 0;
  int test = 0;

  do {
    test = run_ast(core.test, state, loops);
    int choice = update_state(state);
    if (choice == -1)
      return out;
    if (choice == 1)
      continue;

    if (test)
      break;

    out = run_ast(core.body, state, loops + 1);
    choice = update_state(state);
    if (choice == -1)
      return out;
  } while (!test);

  return out;
}

static int run_if(struct astif core, int *state, int loops)
{
  int out = run_ast(core.test, state, loops);
  if (*state)
    return out;

  if (!out)
    out = run_ast(core.then_body, state, loops);
  else
  {
    if (core.else_body)
      out = run_ast(core.else_body, state, loops);
  }
  return out;
}

static int run_ope(struct astope core, int *state, int loops)
{
  int out = run_ast(core.left_part, state, loops);
  if (*state)
    return out;

  if ((out && core.tag == TOK_OR) || (!out && core.tag == TOK_AND))
    return run_ast(core.right_part, state, loops);
  return out;
}

static int run_delim(struct astdelim core, int *state, int loops)
{
  return run_ast(core.body, state, loops);
}

static int run_case(struct astcase core, int *state, int loops)
{
  struct clause *tmp = core.head;
  do {
    for (unsigned ite = 0; *(tmp->test + ite); ite++)
    {
      if (!fnmatch(*(tmp->test + ite), core.word, 0))
        return run_ast(tmp->body, state, loops);
    }

    tmp = tmp->next;
  } while (tmp);
  return 0;
}

int run_looptest(struct ast_shell *ast, int *state, int loops)
{
  int out = 0;
  switch (ast->tag)
  {
  case A_FOR:
    out = run_for(ast->core.astfor, state, loops);
    break;
  case A_WHILE:
    out = run_while(ast->core.astwhile, state, loops);
    break;
  case A_IF:
    out = run_if(ast->core.astif, state, loops);
    break;
  case A_OPE:
    out = run_ope(ast->core.astope, state, loops);
    break;
  case A_DELIM:
    out = run_delim(ast->core.astdelim, state, loops);
    break;
  case A_CASE:
    out = run_case(ast->core.astcase, state, loops);
    break;
  default:
    printf("ERROR BAD TAG: %d\n", ast->tag);
  }
  return out;
}
