# include <err.h>
# include <stdio.h>
# include <stdlib.h>
# include <string.h>
# include <sys/types.h>
# include <sys/wait.h>
# include <unistd.h>

# include "builtin.h"
# include "str.h"
# include "shell42.h"
# include "stock_func.h"
# include "ast_shell.h"
# include "lexer.h"
# include "variable.h"
# include "expand.h"
# include "looptest_run.h"
# include "astfree.h"
# include "run_ast.h"

extern struct info g_info;

static int run_assig(struct astassig core)
{
  char *value = expand_string(core.value);
  add_var(core.var, value, VARIABLE, 0);
  free(value);
  return 0;
}

static int run_func(struct astfunc core)
{
  add_func(core.name, core.body);
  return 0;
}

static int exec_func(char **args, unsigned argc, int *state, int loops)
{
  g_info.depth += 1;
  if (g_info.depth > 1000)
  {
    fprintf(stderr, "42sh: max depth reach\n");
    *state = -2;
    return 1;
  }
  for (unsigned ite = 1; ite < argc + 1; ite++)
  {
    char *etiq = uint_to_str(ite);
    add_var(etiq, args[ite], VARIABLE, 0);
    free(etiq);
  }

  struct ast_shell *func = which_func(*args);
  free_array(args, -1);
  int out = run_ast(func, state, loops);
  g_info.depth -= 1;
  return out;
}

static int exec_cmd(pid_t pid, char **cmd, unsigned decal)
{
  if (pid == 0)
  {
    int out = execvp(cmd[decal], cmd + decal);
    if (out == -1)
    {
      fprintf(stderr, "42sh: %s: command not found\n", cmd[decal]);
      free_ast_shell(g_info.ast, 1);
      free_var_list(g_info.v_list);
      free_func(g_info.func);
      free_array(cmd, -1);
      exit(127);
    }
    exit(out);
  }
  free_array(cmd, -1);
  int stat = 0;
  do {
    waitpid(pid, &stat, WUNTRACED);
  } while (!WIFEXITED(stat) && !WIFSIGNALED(stat));
  if (WIFSIGNALED(stat))
  {
    switch (WTERMSIG(stat))
    {
    case 11:
      fprintf(stderr, "Segmentation fault (core dumped)\n");
      return 139;

    default:
      printf("TYPE SIG:%d\n", WTERMSIG(stat));
    }
    return 139;
  }
  return WEXITSTATUS(stat);
}

int run_cmd(struct astcmd core, int *state, int loops, int force)
{
  char **cmd = build_words_expansion(core.cmd, 1);
  int nb = 0;
  while (cmd[nb])
    nb++;

  unsigned decal = 0;
  while (cmd[decal] && !strlen(cmd[decal]))
    decal++;
  if (decal == core.argc)
    return 0;

  if (which_func(cmd[decal]) && !force)
    return exec_func(cmd + decal, nb - decal - 1, state, loops);
  if (get_builtin_id(cmd[decal]) >= 0 && !force)
  {
    int out = exec_builtin(cmd + decal, nb - decal - 1, state, loops);
    free_array(cmd, -1);
    return out;
  }

  pid_t pid = fork();
  if (pid == -1)
  {
    free_array(cmd, -1);
    fprintf(stderr, "42sh: couldn't fork\n");
    return 1;
  }
  return exec_cmd(pid, cmd, decal);
}

static int choose_ast(struct ast_shell *ast, int *state, int loops)
{
  int out = 0;
  switch (ast->tag)
  {
  case A_ASSIG:
    out = run_assig(ast->core.astassig);
    break;
  case A_CMD:
    out = run_cmd(ast->core.astcmd, state, loops, 0);
    break;
  case A_FUNC:
    out = run_func(ast->core.astfunc);
    break;
  case A_LINE:
    break;
  default:
    out = run_looptest(ast, state, loops);
  }

  if (ast->neg)
    out = (out) ? 0 : 1;

  char *nb_out = calloc(12, 1);
  if (!nb_out)
    err(1, "Failed allocation");
  sprintf(nb_out, "%d", out);

  add_var("?", nb_out, VARIABLE, 0);
  free(nb_out);
  return out;
}

int run_ast(struct ast_shell *ast, int *state, int loops)
{
  if (!ast)
    return 0;

  int out;
  do {
    if (g_info.stop)
      *state = -2;
    out = choose_ast(ast, state, loops);
    if (*state)
      return out;
    ast = ast->next;
  } while (ast);

  return out;
}
