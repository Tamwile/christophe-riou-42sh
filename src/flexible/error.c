# include <stdio.h>

# include "ast_shell.h"
# include "read_char.h"
# include "name.h"
# include "error.h"

extern struct source g_src;

void print_err_tok(int tok)
{
  fprintf(stderr, "42sh: syntax error near unexpected token '%s'\n",
          get_token_name(tok));
  g_src.error = 1;
  if (g_src.tag == INTERACTIVE)
    g_src.error = 2;
}

int unexpected(int tag, int expect1, int expect2)
{
  if (tag == expect1 || tag == expect2)
    return 0;

  int error[10] =
  {
    A_DONE, A_FI, A_CLOSE,
    A_BRACKCL, A_ELSE, A_THEN,
    A_ELIF, A_ESUALC, A_ESAC,
    A_DO
  };

  char *name[10] =
  {
    "done", "fi", ")", "}", "else", "then", "elif", ";;", "esac", "do"
  };

  for (unsigned ite = 0; ite < 10; ite++)
  {
    if (tag == error[ite])
    {
      g_src.error = 1;
      if (g_src.tag == INTERACTIVE)
        g_src.error = 2;
      fprintf(stderr, "42sh: syntax error near unexpected token '%s'\n",
              *(name + ite));
      return 1;
    }
  }
  return 0;
}
