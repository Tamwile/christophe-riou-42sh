# include "name.h"

char *get_token_name(int tok)
{
  char *token[] =
  {
    "for",      "in",
    "do",       "while",
    "until",    "done",
    "if",       "then",
    "else",     "fi",
    "(",        ")",
    "&&",       "||",
    ";",        "!",
    ";;",       "|",
    "case",     "esac",
    "function", "{",
    "}",        "()",
    "elif",     "&"
  };

  switch (tok)
  {
  case -6:
    return "hide_function";
  case -5:
    return "assignation";
  case -4:
    return "void";
  case -3:
    return "newline";
  case -2:
    return "space";
  case -1:
    return "word";
  default:
    return token[tok];
  }
}

int check_var(const char *var)
{
  if (!var || !*var)
    return 0;
  if (*var >= '0' && *var <= '9' )
    return 0;

  for (; *var; var++)
  {
    if (*var < 'a' && *var > 'z' && *var < 'A' && *var > 'Z' && *var < '0'
	&& *var > '9')
      return 0;
  }
  return 1;
}
