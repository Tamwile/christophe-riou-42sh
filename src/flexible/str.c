# include <err.h>
# include <string.h>
# include <stdlib.h>

# include <stdio.h>

# include "ast_shell.h"
# include "read_char.h"
# include "lexer.h"
# include "str.h"

extern struct source g_src;

char *save_str(char *s)
{
  if (!s)
    return NULL;
  unsigned len = strlen(s);
  char *save = calloc(len + 1, 1);
  if (!save)
    err(1, "failed allocation");
  for (unsigned ite = 0; ite < len; ite++)
    *(save + ite) = s[ite];
  return save;
}

char *uint_to_str(unsigned nb)
{
  unsigned tmp = nb;
  unsigned log = 0;
  for (; tmp >= 1; tmp /= 10)
    log++;

  if (!nb)
    log++;

  char *out = malloc(log + 1);
  if (!out)
    err(1, "failed allocation");

  if (!nb)
  {
    out[0] = '0';
    out[1] = 0;
    return out;
  }

  for (unsigned ite = 0; ite < log; ite++)
  {
    out[log - ite - 1] = nb % 10 + '0';
    nb = (nb - nb % 10) / 10;
  }
  out[log] = 0;
  return out;
}

int is_equality(const char *s, int var)
{
  if (!s || strlen(s) < 2)
    return 1;

  int equal = 0;
  int valid = 1;

  if (var && *s >= '0' && *s <= '9')
    valid = 0;

  for (unsigned ite = 0; *s && valid; s++)
  {
    if (*s == '=' && !ite)
    {
      valid = 0;
      break;
    }
    if (*s == '=')
    {
      equal = 1;
      break;
    }
    if (*s == ' ')
      valid = 0;
    if (var && ((*s <= 'a' && *s >= 'z') || (*s <= 'A' && *s >= 'Z') ||
                (*s <= '0' && *s >= '9')))
      valid = 0;
    ite++;
  }

  if (!equal)
    return 1;
  if (equal && !valid)
    return -1;
  return 0;
}

struct astassig cut_equality(char *s)
{
  struct astassig out;
  out.var = calloc(1, strlen(s));
  out.value = calloc(1, strlen(s));
  if (!out.var || !out.value)
    err(1, "Failed allocation");

  unsigned ite = 0;
  for (; s[ite] != '='; ite++)
    out.var[ite] = s[ite];

  ite++;
  for (unsigned i = 0; s[ite]; ite++)
  {
    out.value[i] = s[ite];
    i++;
  }

  return out;
}

int is_func(void)
{
  if (g_src.ahead != '(')
    return 0;

  int tok = get_next_token(NULL, 0, 0);
  mem_token(tok);
  return tok == TOK_PARENTH;
}
