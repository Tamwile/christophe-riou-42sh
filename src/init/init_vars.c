#include <stdio.h>
#include <stdlib.h>

# include "str.h"
# include "variable.h"
# include "ast_shell.h"
# include "init_vars.h"


extern char** environ;

void init_export(void)
{
  unsigned i = 0;
  struct astassig out;
  for (i = 0; environ[i]; i++)
  {
    out = cut_equality(environ[i]);
    add_var(out.var, out.value, VARIABLE, 1);
    free(out.var);
    free(out.value);
  }
}
