# include <stdlib.h>
# include <string.h>

# include "str.h"
# include "variable.h"
# include "alias.h"

char *aliasing(char *word)
{
  char *new = save_str(word);

  char *tmp = get_value(word, ALIAS);
  while (tmp)
  {
    if (!strcmp(word, tmp))
      return word;

    free(new);
    new = tmp;
    tmp = get_value(tmp, ALIAS);
  }

  free(word);
  return new;
}
