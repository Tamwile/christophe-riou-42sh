# include <err.h>
# include <stdio.h>
# include <stdlib.h>
# include <string.h>

# include "alias.h"
# include "shell42.h"
# include "ast_shell.h"
# include "str.h"
# include "name.h"
# include "variable.h"
# include "read_char.h"
# include "lexer.h"

extern struct info g_info;
extern struct source g_src;

/**
 * \fn stop_word(char cara)
 * \brief should the creation of the word be stopped ?
 *
 * \param cara next possible element of the word
 *
 * \return 1 if it should be stopped, 0 otherwise
 */
static int stop_word(char cara)
{
  if (!cara)
    return 1;//g_src.tag != INTERACTIVE;

  char tab[] =
  {
    ' ', '\t', '\n', ';', '&', '|', '(', ')', 0
  };

  for (unsigned ite = 0; tab[ite]; ite++)
  {
    if (cara == tab[ite])
      return 1;
  }
  return 0;
}

/**
 * \fn read_operator(int nonblock, char *token, unsigned cursor)
 * \brief create special operator composed of '&' and '|'
 *
 * \param nonblock should the PS2 be prompt if the reading of token stop
 * \param token begining of the word to create
 * \param cursor number of char already added in token
 *
 * \return the operator created
 */
static char *read_operator(int nonblock, char *token, unsigned cursor)
{
  int max = 0;
  do {
    token[cursor] = get_next_char(nonblock);
    cursor++;

    token = realloc(token, cursor + 1);
    if (!token)
      err(1, "Failed allocation");
    max++;
  } while (token[cursor - 1] == g_src.ahead && max < 2);
  token[cursor] = 0;

  //printf("TOKEN:'%s'\n", token);
  return token;
}

/**
 * \fn get_next_word(int nonblock)
 * \brief create the next word with get_next_char()
 *
 * \param nonblock should the PS2 be prompt if the reading of token stop
 *
 * \return the word created
 */
static char *get_next_word(int nonblock)
{
  unsigned cursor = 0;
  char delim = 0;
  char *token = calloc(3, 1);
  if (!token)
    err(1, "Failed allocation");

  if (g_src.tag == INTERACTIVE)
    actualise();
  while (g_src.ahead == ' ' || g_src.ahead == '\t')
  {
    token[cursor] = get_next_char(nonblock);
  }

  if (g_src.tag == INTERACTIVE)
    actualise();

  if (g_src.ahead == '#')
  {
    trash_all(1);
    return token;
  }

  if ((g_src.ahead == '&' || g_src.ahead == '|' || g_src.ahead == ';')
      && !token[cursor])
    return read_operator(nonblock, token, cursor);

  if (!token[cursor] && stop_word(g_src.ahead))
  {
    token[cursor] = get_next_char(nonblock);

    if (token[cursor] == '(' && g_src.ahead == ')')
      token[cursor + 1] = get_next_char(nonblock);
    return token;
  }

  while (g_src.ahead && (!stop_word(g_src.ahead) || delim))
  {
    token[cursor] = get_next_char(nonblock);
    cursor++;
    token = realloc(token, cursor + 1);
    if (!token)
      err(1, "Failed allocation");

    if (g_src.tag == INTERACTIVE)
      actualise();

    if (token[cursor - 1] == delim)
      delim = 0;
    else if (token[cursor - 1] == '"' || token[cursor - 1] == '\''
             || token[cursor - 1] == '`')
      delim = token[cursor - 1];
  }

  token[cursor] = 0;
  return token;
}

/**
 * \fn get_token_id(const char *s)
 * \brief assossiate the string s to its id token
 *
 * \param s the string to analyze
 * \return id assossiated to s
 */
static int get_token_id(const char *s)
{
  if (!is_equality(s, 1))
    return TOK_ASSIG;
  if (!*s)
    return TOK_VOID;
  int empty = 1;
  unsigned ite;
  for (ite = 0; *(s + ite); ite++)
  {
    if (*(s + ite) != ' ')
    {
      empty = 0;
      break;
    }
  }
  if (empty)
    return TOK_SPACE;
  if (*s == '\n')
    return TOK_LINE;

  char tab[26][9] =
  {
    "for",      "in",
    "do",       "while",
    "until",    "done",
    "if",       "then",
    "else",     "fi",
    "(",        ")",
    "&&",       "||",
    ";",        "!",
    ";;",       "|",
    "case",     "esac",
    "function", "{",
    "}",        "()",
    "elif",     "&"
  };

  for (int ite = 0; ite < 26; ite++)
  {
    if (!strcmp(tab[ite], s))
      return ite;
  }

  if (is_func())
    return TOK_HIDE_FUNC;
  return -1;
}

int get_next_token(char **word, int nonblock, int raw)
{
  if (g_src.mem_tok != TOK_VOID)
  {
    int tmp = g_src.mem_tok;
    g_src.mem_tok = TOK_VOID;
    return tmp;
  }

  if (g_src.tag == INTERACTIVE)
    actualise();

  char *val = get_next_word(nonblock);
  int id = get_token_id(val);
  if (id != TOK_WORD && id != TOK_ASSIG && id != TOK_HIDE_FUNC && !raw)
    free(val);

  if (id == -1)
  {
    val = aliasing(val);

    int id = get_token_id(val);
    if ((id != TOK_WORD && id != TOK_ASSIG && id != TOK_HIDE_FUNC && !raw) ||
	!word)
      free(val);
  }
  if (word)
    *word = val;
  return id;
}

char **get_full_word(char *next, unsigned *nb)
{
  char **out = malloc(2 * sizeof (char *));
  if (!out)
    err(1, "Failed allocation");

  out[0] = next;
  *nb = 1;

  while (g_src.ahead != '\n' && g_src.ahead != '\0' && g_src.ahead != ';' &&
	 g_src.ahead != '&' && g_src.ahead != '|' && g_src.ahead != ')' &&
	 g_src.ahead != '}' && g_src.ahead)
  {
    next = get_next_word(1);
    int tok = get_token_id(next);
    if (tok == TOK_VOID || tok == TOK_SPACE)
    {
      free(next);
      continue;
    }

    out = realloc(out, (*nb + 2) * sizeof (char *));
    if (!out)
      err(1, "Failled allocation");
    out[*nb] = next;
    *nb += 1;
  }

  out[*nb] = 0;
  return out;
}

char *get_next_var(int error)
{
  char *var;
  get_next_token(&var, 1, 1);

  if (!check_var(var))
  {
    if (error)
    {
      fprintf(stderr, "42sh: '%s': not a valid identifier\n", var);
      g_src.error = 1;
    }
    free(var);
    return NULL;
  }

  return var;
}
