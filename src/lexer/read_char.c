# include <err.h>
# include <stdio.h>
# include <stdlib.h>
# include <string.h>

# include "ast_shell.h"
# include "variable.h"
# include "read_char.h"

struct source g_src =
{
  .tag = INTERACTIVE,
  .file = NULL,
  .cmd = NULL,
  .cursor = 0,
  .end = 0,
  .ahead = 0,
  .mem_tok = -4,
  .buf = NULL,
  .error = 0
};

static char get_next_char_wrap(int nonblock)
{
  if (g_src.ahead == '\n' && g_src.tag == INTERACTIVE && nonblock)
  {
    char *ps2 = get_value("PS2", VARIABLE);
    printf("%s", ps2);
    free(ps2);
  }

  char out = g_src.ahead;
  if (g_src.tag == INTERACTIVE && g_src.ahead == '\n')
  {
    g_src.ahead = 0;
    return out;
  }

  if (g_src.end)
    return 0;

  switch (g_src.tag)
  {
  case FILE_SRC:
    g_src.ahead = fgetc(g_src.file);
    if (g_src.ahead == EOF)
      g_src.end = 1;
    break;
  case INTERACTIVE:
    g_src.ahead = fgetc(stdin);
    break;
  case CMD_SRC:
    g_src.cursor++;
    g_src.ahead = g_src.cmd[g_src.cursor - 1];
    if (!g_src.ahead)
      g_src.end = 1;
  }

  if (!out && g_src.tag == INTERACTIVE)
  {
    if (!g_src.ahead)
      return get_next_char(nonblock);
    out = g_src.ahead;
    g_src.ahead = 0;
  }
  return out;
}

char get_next_char(int nonblock)
{
  char out = get_next_char_wrap(nonblock);

  if (!g_src.buf)
  {
    g_src.buf = calloc(2, 1);
    if (!g_src.buf)
      err(1, "Failed allocation");

    *g_src.buf = out;
    return out;
  }

  unsigned nb = strlen(g_src.buf);
  g_src.buf = realloc(g_src.buf, nb + 2);
  if (!g_src.buf)
    err(1, "Failed allocation");

  g_src.buf[nb] = out;
  g_src.buf[nb + 1] = 0;
  return out;
}

void actualise(void)
{
  if (!g_src.ahead)
    g_src.ahead = fgetc(stdin);
}

void mem_token(int tok)
{
  g_src.mem_tok = tok;
}

void trash_all(int com)
{
  if (com)
  {
    while (g_src.ahead && g_src.ahead != '\n')
      get_next_char(0);
    return;
  }

  while (g_src.ahead != 0)
    get_next_char(0);
  if (!g_src.error)
    g_src.ahead = '\n';
}
