# include <err.h>
# include <stdio.h>
# include <string.h>
# include <stdlib.h>
# include <readline/readline.h>
# include <readline/history.h>

# include "ast_build.h"
# include "ast_shell.h"
# include "astfree.h"
# include "error.h"
# include "print_dot.h"
# include "read_char.h"
# include "run_ast.h"
# include "signals.h"
# include "stock_func.h"
# include "str.h"
# include "variable.h"
# include "init_vars.h"

# include "shell42.h"

struct info g_info =
{
  .norc = 0,
  .ast_print = 0,
  .version = 0,
  .v_list = NULL,
  .func = NULL,
  .ast = NULL,
  .src = 0,
  .stop = 1,
  .depth = 0
};

extern struct source g_src;

/**
 * \fn diff(const char *s1, const char *s2)
 * \brief determine if the strings are differents
 *
 * \param s1 first string
 * \param s2 second string
 * \return 0 if they are the same, positif if s2 is the begining of s1, negatif
 * otherwise
 */
static int diff(const char *s1, const char *s2)
{
  int same = 1;
  unsigned ite = 1;
  for (; *(s1 + ite) && *(s2 + ite); ite++)
  {
    if (*(s1 + ite) != *(s2 + ite))
    {
      same = 0;
      break;
    }
  }
  if (same && !*(s1 + ite) && !*(s2 + ite))
    return 0;
  if (*(s1 + ite) && !*(s2 + ite))
    return ite;
  return -ite;
}

/**
 * \fn get_option_id(const char *s)
 * \brief determine the id of the option
 *
 * \param s option to determine
 * \return id of the option
 */
static int get_option_id(const char *s)
{
  char tab[4][11] =
  {
    "--version",
    "--ast-print",
    "--norc",
    "-c",
  };

  int maxi = 0;
  int first = 0;
  int id = -1;
  for (int ite = 0; ite < 4; ite++)
  {
    int tmp = diff(tab[ite], s);
    if (!tmp)
      return ite;
    if (tmp == maxi)
      first = 0;
    if (tmp > maxi)
    {
      first = 1;
      maxi = tmp;
      id = ite;
    }
  }
  if (first)
    return id;
  return -1;
}

/**
 * \fn read_arg(struct var_list *v_list, unsigned nb, char *argv)
 * \brief fill the list of variable and load the script file if there is one.
 *
 * \param v_list list of variable
 * \param nb number of arguments already in the list
 * \param argv arg to add to the list or script to load
 * \return the new list
 */
static void read_arg(unsigned nb, char *argv)
{
  if (g_src.tag == INTERACTIVE)
  {
    g_src.tag = FILE_SRC;
    FILE *f = fopen(argv, "r");
    if (!f)
    {
      fprintf(stderr, "42sh: %s: No such file or directory\n", argv);
      exit(127);
    }
    g_src.file = f;
    return;
  }

  char *etiq = uint_to_str(nb);
  add_var(etiq, argv, VARIABLE, 0);
  free(etiq);
}

/**
 * \fn parse_options(int argc, char *argv[])
 * \brief parse the option of ./42sh
 *
 * \param argc number of arguments
 * \param argv name of binary and arguments put at execution
 * \return struct about the source of execution
 */
static void parse_options(int argc, char *argv[])
{
  argc--;
  argv++;
  int long_opt = 1;
  int nb = 0;
  while (argc)
  {
    switch (get_option_id(*argv) * long_opt + long_opt - 1)
    {
    case 0:
      printf("Version 1.0\n");
      g_info.version = 1;
      return;
    case 1:
      g_info.ast_print = 1;
      break;
    case 2:
       g_info.norc = 1;
      break;
    case 3:
      g_src.tag = CMD_SRC;
      argv++;
      argc--;
      if (argc < 1)
      {
        fprintf(stderr, "bash: -c: option requires an argument\n");
        exit(2);
      }
      g_src.cmd = *argv;
      long_opt = 0;
      break;
    case -1:
      long_opt = 0;
      read_arg(nb, *argv);
      nb++;
    }
    argc--;
    argv++;
  }
}

static void load_variable(void)
{
  add_var("IFS", " ", VARIABLE, 0);
  add_var("PS1", "42sh$ ", VARIABLE, 0);
  add_var("PS2", "> ", VARIABLE, 0);
  add_var("?", "0", VARIABLE, 0);
  init_export();
  g_info.src = g_src.tag;
}

static int exec_this_ast(struct ast_shell *ast_shell, int *state)
{
  if (ast_shell)
  {
    if (g_info.ast_print)
      print_dot(ast_shell);

    *state = 0;
    int out = run_ast(ast_shell, state, 0);
    free_ast_shell(ast_shell, 1);
    return out;
  }

  char *ret = uint_to_str(g_src.error);
  add_var("?", ret, VARIABLE, 0);
  free(ret);

  if (g_src.tag != INTERACTIVE)
    *state = -1;

  trash_all(0);
  free(g_src.buf);
  g_src.buf = NULL;
  return g_src.error;
}

/**
 * \fn main(int argc, char *argv[])
 * \brief main of 42sh
 *
 * \param argc number of arguments
 * \param argv name of binary and arguments put at execution
 * \return value of exit
 */
int main(int argc, char *argv[])
{
  parse_options(argc, argv);
  if (g_info.version)
    return 0;
  load_variable();

  if (g_src.tag != INTERACTIVE)
    get_next_char(0);

  signal_listenning();
  int state = 0;
  int out = 0;
  do {
    g_src.error = 0;
    struct ast_shell *ast_shell = build_ast_shell(1, 0);
    //add_history(g_src.buf);
    free(g_src.buf);
    g_src.buf = NULL;
    if (ast_shell && unexpected(ast_shell->tag, -1, -1))
    {
      free(ast_shell);
      ast_shell = NULL;
    }

    g_info.ast = ast_shell;
    g_info.stop = 0;
    g_info.depth = 0;
    out = exec_this_ast(ast_shell, &state);
    g_info.stop = 1;

  } while (state != -1 && g_src.end != 1);

  if (g_src.tag == FILE_SRC)
    fclose(g_src.file);

  free_var_list(g_info.v_list);
  free_func(g_info.func);
  return out;
}
