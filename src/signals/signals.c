# include <stdlib.h>
# include <stdio.h>
# include <signal.h>

# include "shell42.h"
# include "read_char.h"
# include "variable.h"
# include "signals.h"

extern struct info g_info;

static void handler(int signum)
{
  signum = signum;
  trash_all(0);
  printf("\n");

  if (g_info.stop)
  {
    char *ps1 = get_value("PS1", VARIABLE);
    printf(ps1);
    free(ps1);
  }
  g_info.stop = 1;
}

void signal_listenning(void)
{
  struct sigaction act;
  act.sa_handler = handler;
  act.sa_flags = 0;
  sigemptyset(&act.sa_mask);
  sigaction(SIGINT, &act, NULL);
}
