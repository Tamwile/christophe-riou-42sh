# include <err.h>
# include <string.h>
# include <stdlib.h>
# include <stdio.h>

# include "str.h"
# include "variable.h"
# include "ast_shell.h"
# include "astfree.h"
# include "expand.h"

/**
 * \fn fusion(char *out, char *b)
 * \brief concat the allocated string 'out' and 'b' and free 'b'
 *
 * \param out first string to concat
 * \param b second string to concat and free
 *
 * \return the string allocated of out + b
 */
static char *fusion(char *out, char *b)
{
  unsigned len_a = (out) ? strlen(out) : 0;
  unsigned len = (b) ? len_a + strlen(b) : len_a;

  out = realloc(out, len + 1);
  if (!out)
    err(1, "failed memory allocation");

  for (unsigned ite = len_a; ite < len; ite++)
    out[ite] = b[ite - len_a];
  out[len] = '\0';
  free(b);
  return out;
}

/**
 * \fn build_var(char **str, int *end)
 * \brief build the next variable writed in str
 *
 * \param str where the variable come from, is updated when read
 * \param end is the var ready to be printed ?
 *
 * \return the variable extracted from str
 */
static char *build_var(char **str, int *end)
{
  char *var = calloc(2, 1);
  if (!var)
    err(1, "Failed allocation");

  unsigned ite = 0;
  *str += 1;
  if (!**str || **str == ' ')
  {
    var[ite] = '$';
    *end = 1;
    return var;
  }

  while (**str && **str != ' ' && **str != '\'' && **str != '"' && **str != '`')
  {
    var = realloc(var, ite + 2);
    if (!var)
      err(1, "Failed allocation");
    var[ite] = **str;
    *str += 1;
    ite++;
  }

  var[ite] = 0;
  return var;
}

static char *build_var_stat(char *str, int *nb)
{
  char *var = calloc(2, 1);
  if (!var)
    err(1, "Failed allocation");

  unsigned ite = 0;
  if (!str[ite + 1] || str[ite + 1] == ' ')
  {
    var[ite] = '$';
    *nb = 1;
    return var;
  }

  while (*(str + ite + 1) && str[ite + 1] != ' ' &&  str[ite + 1] != '\'' &&
         str[ite + 1] != '"' && str[ite + 1] != '`' && str[ite + 1] != '$')
  {
    var = realloc(var, ite + 2);
    if (!var)
      err(1, "Failed allocation");
    var[ite] = str[ite + 1];
    ite++;
  }

  *nb = ite + 1;
  var[ite] = 0;
  return var;
}

static char *exp_new(char *val)
{
  if (!val)
    return NULL;
  int end = 1;
  do {
    for (unsigned ite = 0; val[ite]; ite++)
    {
      if (val[ite] == '$')
      {
        int nb = 0;
        char *copy = val;
        copy = save_str(copy);
        copy[ite] = 0;
        char *var = build_var_stat(val + ite, &nb);
        char *end = save_str(val + ite + nb);
        free(val);
        copy = fusion(copy, var);
        copy = fusion(copy, end);
        val = copy;
        end = 0;
      }
    }
  } while (!end);
  return val;
}

static char **append_cut(char **str, char **expand, int *cursor)
{
  int end = 0;
  char *var = build_var(str, &end);

  char **out = calloc(sizeof (char *), 2);
  if (!out)
    err(1, "Failed allocation");

  if (end)
  {
    *expand = fusion(*expand, var);
    *cursor += 1;
    out[0] = *expand;
    *cursor = 0;
    *expand = calloc(2, 1);
    if (!*expand)
      err(1, "Failled allocation");
    return out;
  }

  char *value = get_value(var, VARIABLE);
  free(var);
  value = exp_new(value);
  if (value)
    *cursor += strlen(value);
  *expand = fusion(*expand, value);
  out[0] = *expand;
  *cursor = 0;
  *expand = calloc(2, 1);
  if (!*expand)
    err(1, "Failled allocation");
  return out;
}

static char *append_pact(char **str, char *expand, int *cursor)
{
  int end = 0;
  char *var = build_var(str, &end);

  if (end)
  {
    expand = fusion(expand, var);
    *cursor += 1;
    return expand;
  }

  char *value = get_value(var, VARIABLE);
  free(var);
  value = exp_new(value);
  if (value)
    *cursor += strlen(value);
  expand = fusion(expand, value);
  return expand;
}

static char **append_space(char **str, char **expand, int *cursor)
{
  char **out = calloc(sizeof (char *), 2);
  if (!out)
    err(1, "Failed allocation");

  *str += 1;
  (*expand)[*cursor] = 0;
  *cursor = 0;
  out[0] = *expand;
  return out;
}

char **merge(char **a, char **b)
{
  if (!b)
    return a;

  unsigned lena = 0;
  if (a)
  {
    for (; a[lena]; lena++)
      continue;
  }

  a = realloc(a, (lena + 1) * sizeof (char *));
  if (!a)
    err(1, "Failed allocation");
  for (unsigned ite = 0; b[ite]; ite++)
  {
    a = realloc(a, (lena + ite + 2) * sizeof (char *));
    if (!a)
      err(1, "Failed allocation");

    a[lena] = b[ite];
    lena++;
  }

  a[lena] = NULL;
  free(b);
  return a;
}

char **expand_variable(char *str, int erase)
{
  char **out = calloc(sizeof (char *), 2);
  char *expand = calloc(1, 2);
  if (!expand || !out)
    err(1 , "Failed allocation");

  int cursor = 0;
  char delim = 0;
  for (; *str; str++)
  {
    if (*str == delim)
    {
      delim = 0;
      if (erase)
        continue;
    }
    else
    {
      if ((*str == '"' || *str == '\'' || *str == '`') && !delim)
      {
        delim = *str;
        if (erase)
          continue;
      }
    }
    if (*str == '$' && !delim)
    {
      out = merge(out, append_cut(&str, &expand, &cursor));
      if (!*str)
        break;
      continue;
    }
    if (*str == '$' && delim == '"')
    {
      expand = append_pact(&str, expand, &cursor);
      if (!*str)
        break;
      continue;
    }
    if (*str == ' ' && !delim)
    {
      out = merge(out, append_space(&str, &expand, &cursor));
      if (!*str)
        break;
      continue;
    }

    expand = realloc(expand, cursor + 2);
    if (!expand)
      err(1, "Failed allocation");
    expand[cursor] = *str;
    cursor++;
    expand[cursor] = 0;
  }
  return merge(out, append_space(&str, &expand, &cursor));
}

char **build_words_expansion(char **args, int erase)
{
  char **out = NULL;
  for (; *args; args++)
    out = merge(out, expand_variable(*args, erase));

  return out;
}

char *expand_string(char *str)
{
  char *out = calloc(1, 2);
  char **tab = calloc(sizeof (char *), 2);
  if (!tab || !out)
    err(1, "Failed allocation");

  tab[0] = str;
  char **output = build_words_expansion(tab, 1);
  free(tab);

  for (unsigned ite = 0; output[ite]; ite++)
    out = fusion(out, output[ite]);
  free(output);
  return out;
}
