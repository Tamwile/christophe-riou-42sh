# include <err.h>
# include <string.h>
# include <stdlib.h>

# include "ast_shell.h"
# include "astfree.h"
# include "shell42.h"
# include "stock_func.h"

extern struct info g_info;

struct ast_shell *which_func(char *func)
{
  struct func_list *tmp = g_info.func;
  if (!tmp)
    return NULL;

  for (; tmp; tmp = tmp->next)
  {
    if (!strcmp(tmp->name, func))
      return tmp->func;
  }

  return NULL;
}

static struct func_list *init_func(char *name, struct ast_shell *body)
{
  struct func_list *out = calloc(sizeof (struct func_list), 1);
  if (!out)
    err(1, "Failed allocation");

  out->name = name;
  out->func = body;
  return out;
}

void add_func(char *func, struct ast_shell *body)
{
  if (!g_info.func)
  {
    g_info.func = init_func(func, body);
    return;
  }

  struct func_list *tmp = g_info.func;
  for (; tmp->next; tmp = tmp->next)
  {
    if (!strcmp(tmp->name, func))
    {
      free(tmp->name);
      free_ast_shell(tmp->func, 0);
      tmp->name = func;
      tmp->func = body;
      return;
    }
  }

  if (!strcmp(tmp->name, func))
  {
    free(tmp->name);
    free_ast_shell(tmp->func, 0);
    tmp->name = func;
    tmp->func = body;
    return;
  }

  tmp->next = init_func(func, body);
}

void free_func(struct func_list *list)
{
  while (list)
  {
    free(list->name);
    free_ast_shell(list->func, 0);
    struct func_list *tmp = list;
    list = list->next;
    free(tmp);
  }
}
