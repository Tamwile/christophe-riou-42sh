#include <err.h>
#include <stdlib.h>
#include <string.h>

#include "str.h"
#include "shell42.h"
#include "variable.h"

extern struct info g_info;

static struct var_list *get_v_list(struct var_list *v_list, char *etiquette,
                            enum var_type type)
{
  while (v_list)
  {
    if ((strcmp(v_list->etiquette, etiquette) == 0) && (v_list->type == type))
      return v_list;
    v_list = v_list->next;
  }
  return NULL;
}

void add_var(char *etiquette, char *value, enum var_type type, int export)
{
  struct var_list *v_list = g_info.v_list;
  struct var_list *test = get_v_list(v_list, etiquette, type);
  if (test)
  {
    free(test->value);
    test->value = save_str(value);
    test->exported = export;
    g_info.v_list = v_list;
  }
  else
  {
    struct var_list *v_list2 = malloc(sizeof(struct var_list));
    if (! v_list2)
      err(1, "Failed allocation");
    v_list2->etiquette = save_str(etiquette);
    v_list2->value = save_str(value);
    v_list2->type = type;
    v_list2->exported = export;
    v_list2->next = v_list;
    g_info.v_list = v_list2;
  }
}

char *get_value(char *etiquette, enum var_type type)
{
  struct var_list *test = get_v_list(g_info.v_list, etiquette, type);
  if (!test)
    return NULL;

  return save_str(test->value);
}

void free_var_list(struct var_list *v_list)
{
  if (v_list)
  {
    if (v_list->next)
      free_var_list(v_list->next);
    free(v_list->etiquette);
    free(v_list->value);
    free(v_list);
  }
}

struct var_list *pop_alias(struct var_list *list, char *alias)
{
  if (!list)
    return NULL;

  struct var_list *tmp = list;
  if (!strcmp(list->etiquette, alias))
  {
    tmp = tmp->next;
    free(list->etiquette);
    free(list->value);
    free(list);
    return tmp;
  }

  for (; tmp->next; tmp = tmp->next)
  {
    if (!strcmp(tmp->next->etiquette, alias))
    {
      struct var_list *new = tmp->next->next;
      free(tmp->next->etiquette);
      free(tmp->next->value);
      free(tmp->next);
      tmp->next = new;
      return list;
    }
  }
  return list;
}

struct var_list *unalias_all(struct var_list *list)
{
  struct var_list *tmp = list;
  while (tmp)
  {
    struct var_list *next = tmp->next;
    if (tmp->type == ALIAS)
      list = pop_alias(list, tmp->etiquette);
    tmp = next;
  }
  return list;
}
