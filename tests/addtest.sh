#!/bin/bash

if [ ! "$#" -eq 3 ]; then
    echo "usage: ./addtest.sh type test_name \"args\""
    exit 1
fi

folder="test/"

if [ ! -e "$folder""$1" ]; then
    echo "the folder $1 doesn't exist"
    exit 2
fi

cd "folder_test"
file="../""$folder""$1""/""$2"

#echo "$1" > $file
echo "$3" > $file
bash --posix -c "$3" 1> /dev/null 2> err

cara=$(wc -c < err)
rm err

if [ "$cara" -eq 0 ]; then
    echo 1 >> $file
else
    echo 0 >> $file
fi

bash --posix -c "$3" 1> /dev/null 2> /dev/null
echo "$?" >> $file
bash --posix -c "$3" 1>> $file 2> /dev/null

cd ../
