import os
import time
import subprocess
import sys

test_dir="../test"
my_exec = "../../build/42sh"

def test(arg, com, out, error, return_value, option):
  com = "\'" + com + "\' "
  my_list = [my_exec, "-c", arg]
  my = subprocess.Popen(my_list, stdout=subprocess.PIPE,
                  stderr=subprocess.PIPE)
  k = 0
  while my.poll() is None and k < 1000:
    time.sleep(0.001)
    k += 1
  if my.poll() is None:
    my.terminate()
    com += "\033[33mTIMEOUT\033[0m"
    print(com)
    return 0
  my_stdout = ""
  for line in my.stdout:
    my_stdout += line.decode("ascii")
  my_stderr = ""
  for line in my.stderr:
    my_stderr += line.decode("ascii")
  i = 1
  File = open("returned_stdout", 'w')
  File.write(my_stdout)
  File.close()
  File = open("expected_stdout", 'w')
  File.write(out)
  File.close()
  diff = subprocess.run(["diff", "-u", "returned_stdout", "expected_stdout"],
                        stdout=subprocess.PIPE, stderr=subprocess.PIPE)
  File = open("diff_stdout", 'w')
  File.write(diff.stdout.decode("ascii"))
  File.close()
  if (diff.returncode == 1):
    i = 0
  os.remove("returned_stdout")
  os.remove("expected_stdout")
  File = open("returned_value", 'w')
  File.write(str(my.returncode))
  File.close()
  File = open("expected_value", 'w')
  File.write(return_value)
  File.close()
  diff = subprocess.run(["diff", "-u", "returned_value", "expected_value"],
                        stdout=subprocess.PIPE, stderr=subprocess.PIPE)
  File = open("diff_value", 'w')
  File.write(diff.stdout.decode("ascii"))
  File.close()
  if (diff.returncode == 1):
    i = 0
  os.remove("returned_value")
  os.remove("expected_value")
  if (str(error) == "0"):
    i = (i and (my_stderr != ""))
  else:
    i = (i and (my_stderr == ""))
  if (i == 1):
    com += "\033[32mTrue\033[0m"
  else:
    com += "\033[31mFalse\033[0m"
  print(com)
  if (option == "-v"):
    File = open("diff_stdout", 'r')
    list_lines = File.readlines()
    for line in list_lines:
      if (line[0] == '-'):
        print("\t\t\033[31m" + line + "\033[0m", end='')
      elif (line[0] == '+'):
        print("\t\t\033[32m" + line + "\033[0m", end='')
      else:
        print("\t\t" + line, end='')
    if (len(list_lines) != 0):
      print()
    File.close()
    File = open("diff_value", 'r')
    list_lines = File.readlines()
    for line in list_lines:
      if (line[0] == '-'):
        print("\t\t\033[31m" + line + "\033[0m", end='')
      elif (line[0] == '+'):
        print("\t\t\033[32m" + line + "\033[0m", end='')
      else:
        print("\t\t" + line, end='')
    if (len(list_lines) != 0):
      print()
    File.close()
    if ((str(error) == "0") and (i == 0) and my_stderr == ""):
      print("\t\t\033[33mstderr mustn\'t be empty\033[0m\n")
    elif ((str(error) != "0") and (i == 0) and my_stderr != ""):
      print("\t\t\033[33mstderr is not empty\n" + my_stderr + "\033[0m\n")
  os.remove("diff_stdout")
  os.remove("diff_value")
  return i

option = ""
if (len(sys.argv) == 2):
  option = sys.argv[1]

l_dir = []
for element in os.listdir(test_dir):
  if os.path.isdir(test_dir + '/' + element):
    l_dir.append(element)

j = 0
m = 0
for element1 in l_dir:
  if os.path.isdir(test_dir + '/' + element1):
    if (option == "-l"):
      if (element1 != l_dir[len(l_dir) - 1]):
        print("%s, " %element1, end='')
      else:
        print("%s" %element1)
    else:
      print("%s:\n" % element1)
      i = 0
      for element2 in os.listdir(test_dir + '/' + element1):
        print("\t%s:" % element2, end=' ')
        my_file = open(test_dir + '/' + element1 + '/' + element2, "r")
        my_cmd = my_file.readline().rstrip('\n\r')
        my_desciptor = "42sh -c \"" + my_cmd + "\""
        my_error_check = my_file.readline().rstrip('\n\r')
        my_return_value = my_file.readline().rstrip('\n\r')
        my_out = my_file.read()
        my_file.close()
        if (test(my_cmd, my_desciptor, my_out, my_error_check,
                  my_return_value, option) == 1):
          i += 1
          j += 1
        m += 1
      rank = int(i / len(os.listdir(test_dir + '/' + element1)) * 100)
      print("\t[\033[32m%i%%\033[0m] passed" % rank)
      if (element1 != l_dir[len(l_dir) - 1]):
        print()
if (option != "-l"):
  print("\033[6m\n%i /" % j, end =' ')
  print("%i tests passed\033[0m" % m)
